module gitlab.com/jaxnet/core/miner

go 1.15

require (
	github.com/btcsuite/btcd v0.22.0-beta
	github.com/btcsuite/btcutil v1.0.3-0.20201208143702-a53e38424cce
	github.com/davecgh/go-spew v1.1.1
	github.com/fsnotify/fsnotify v1.5.1 // indirect
	github.com/go-redis/redis/v8 v8.11.3
	github.com/google/uuid v1.2.0
	github.com/gorilla/mux v1.7.3
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/pkg/errors v0.9.1
	github.com/prometheus/client_golang v1.11.0
	github.com/rs/zerolog v1.25.0
	gitlab.com/jaxnet/jaxnetd v0.4.4
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)

//	github.com/btcsuite/btcd => ../btcd
//replace gitlab.com/jaxnet/jaxnetd => ../jaxnetd
