/*
 * Copyright (c) 2020 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package communicator

import (
	"context"
	"time"

	btcdwire "github.com/btcsuite/btcd/wire"
	"github.com/patrickmn/go-cache"
	"gitlab.com/jaxnet/core/miner/core/common"
	"gitlab.com/jaxnet/core/miner/core/communicator/events"
	"gitlab.com/jaxnet/core/miner/core/miner/tasks"
	"gitlab.com/jaxnet/core/miner/core/settings"
	"gitlab.com/jaxnet/core/miner/core/utils"
	"gitlab.com/jaxnet/jaxnetd/types/wire"
)

const (
	pollingIntervalSeconds               = 5
	avgBeaconBlockMimingIntervalSeconds  = 60 * 10 // 10 minutes
	avgShardBlockMiningIntervalSeconds   = 15
	avgBitcoinBlockMiningIntervalSeconds = 60 * 10 // 10 minutes

	beaconEventsSlotsCount  = avgBeaconBlockMimingIntervalSeconds / pollingIntervalSeconds
	shardEventsSlotsCount   = avgShardBlockMiningIntervalSeconds / pollingIntervalSeconds
	bitcoinEventsSlotsCount = avgBitcoinBlockMiningIntervalSeconds / pollingIntervalSeconds
	// By default results chan is expected to be non-blocking (buffered channel should be used),
	// to not affect miner with hangs/lags of the RPC interface.
	minerResultsCapacity = 256
)

// Communicator implements main communication layer with jax.net nodes (beacon and all shards).
// It is responsible for collecting block candidates from shards and the beacon for further processing,
// as well as propagating back minded blocks to the related shards or beacon.
//
// Internally implements RPC-polling for fetching block candidates,
// and another one RPC-based interface for pushing blocks back.
type Communicator struct {
	utils.StoppableMixin

	config                 *settings.Configuration
	beaconBlockCandidates  chan events.BeaconBlockCandidate
	shardsBlockCandidates  chan events.ShardBlockCandidate
	bitcoinBlockCandidates chan events.BitcoinBlockCandidate

	// Receives mined blocks from the miner for further transfer to the node (via RPC-interface).
	outgoingBitcoinBlocks chan btcdwire.MsgBlock

	// Receives mined blocks from the miner for further transfer to the node (via RPC-interface).
	outgoingBeaconBlocks chan wire.MsgBlock

	// Map of channels.
	// Each channel receives mined blocks (of the corresponding shard) from the minder,
	// and stores them for further transfer to the node (via RPC-interface).
	outgoingShardsBlocksChannels map[common.ShardID]chan *wire.MsgBlock

	// Cache already send miner results to the node.
	// If miner result was sent to the node then skip duplicate PoW hashes.
	processedMinerResultCache *cache.Cache

	// Channel for stratum server mining.update_block events
	StratumUpdateBlock chan events.StratumUpdateBlock

	// Channel for solved miner jobs
	minerResults chan tasks.MinerResult
}

func New(ctx context.Context, conf *settings.Configuration) (communicator *Communicator) {
	outgoingShardsBlocks := make(map[common.ShardID]chan *wire.MsgBlock)

	for _, shard := range conf.Shards {
		outgoingShardsBlocks[shard.ID] = make(chan *wire.MsgBlock)
	}

	communicator = &Communicator{
		config: conf,

		beaconBlockCandidates:  make(chan events.BeaconBlockCandidate, beaconEventsSlotsCount),
		shardsBlockCandidates:  make(chan events.ShardBlockCandidate, shardEventsSlotsCount*conf.TotalNetworkShardsCount()),
		bitcoinBlockCandidates: make(chan events.BitcoinBlockCandidate, bitcoinEventsSlotsCount),

		outgoingBitcoinBlocks:        make(chan btcdwire.MsgBlock, beaconEventsSlotsCount),
		outgoingBeaconBlocks:         make(chan wire.MsgBlock, beaconEventsSlotsCount),
		outgoingShardsBlocksChannels: outgoingShardsBlocks,
		processedMinerResultCache:    cache.New(avgBeaconBlockMimingIntervalSeconds*time.Minute, (avgBeaconBlockMimingIntervalSeconds+5)*time.Minute),
		StratumUpdateBlock:           make(chan events.StratumUpdateBlock),
		minerResults:                 make(chan tasks.MinerResult, minerResultsCapacity),
	}
	communicator.Ctx = ctx
	communicator.IsStopped = func() bool {
		// There are no commonly used resources, that must be freed
		// before creating next instance of communicator,
		// so, there is no need to wait for internal goroutines to be stopped.
		return true
	}

	return
}

// Run begins polling of blocks from the blockchain node
// and sending of the mined blocks back to the blockchain node.
//
// WARN: Being called blocks the execution flow.
func (c *Communicator) Run() {
	if c.config.BurnBtcReward {
		requestOptions.Capabilities = append(requestOptions.Capabilities, "burnbtcreward")
		requestOptions.Capabilities = append(requestOptions.Capabilities, "burnjaxnetreward")

	} else {
		requestOptions.Capabilities = append(requestOptions.Capabilities, "burnjaxreward")
	}

	// [async]
	c.handleBlockCandidatesPolling()

	// [async]
	// Begin receiving mined beacon blocks and sending them to the beacon chain.
	c.processMinedBitcoinBlocks()

	// [async]
	// Begin receiving mined beacon blocks and sending them to the beacon chain.
	c.processMinedBeaconBlocks()

	// [async]
	// Begin receiving mined shards blocks and sending them to the corresponding shard chain.
	c.processMinedShardsBlocks()

	// [sync] locks current goroutine.
	c.dispatchSolvedBlocks(c.minerResults)
}

// BeaconBlockCandidates returns the channel
// that is populated every time new block candidate arrives from beacon chain.
func (c *Communicator) BeaconBlockCandidates() <-chan events.BeaconBlockCandidate {
	return c.beaconBlockCandidates
}

// ShardsBlockCandidates returns the channel
// that is populated every time new block candidate arrives from some of shard chain(s).
func (c *Communicator) ShardsBlockCandidates() <-chan events.ShardBlockCandidate {
	return c.shardsBlockCandidates
}

// BitcoinBlockCandidates returns the channel
// that is populated every time new block candidate arrives from bitcoin node.
func (c *Communicator) BitcoinBlockCandidates() <-chan events.BitcoinBlockCandidate {
	return c.bitcoinBlockCandidates
}

func (c *Communicator) dispatchSolvedBlocks(results <-chan tasks.MinerResult) {
	for {
		select {
		case <-c.Ctx.Done():
			return
		case minerResults := <-results:
			if minerResults.ContainsMinedBitcoinBlock() {
				c.outgoingBitcoinBlocks <- minerResults.BitcoinBlock
			}

			if minerResults.ContainsMinedBeaconBlock() {
				c.outgoingBeaconBlocks <- minerResults.BeaconBlock
			}

			if minerResults.ContainsMinedShardsBlocks() {
				for shardID, solvedBlock := range minerResults.ShardsBlocks {
					c.outgoingShardsBlocksChannels[shardID] <- solvedBlock
				}
			}
		}
	}
}

// MinerResults returns channel to send miner result.
func (c *Communicator) MinerResults() chan tasks.MinerResult {
	return c.minerResults
}
