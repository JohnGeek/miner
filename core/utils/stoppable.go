/*
 * Copyright (c) 2020 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package utils

import (
	"context"
	"sync"
	"time"
)

const (
	stoppingTTL = time.Millisecond * 50
)

type StoppableMixin struct {
	Ctx context.Context

	MustBeStopped bool

	IsStopped func() bool

	Done chan struct{}
}

func Init(ctx context.Context) (sm StoppableMixin) {
	sm.Ctx = ctx
	sm.Done = make(chan struct{})
	return sm
}

func (sm *StoppableMixin) Finish() {
	sm.Done <- struct{}{}
}

func (sm *StoppableMixin) StopUsing(wg *sync.WaitGroup) {
	sm.MustBeStopped = true

	for {
		// select {
		// case <- sm.Ctx.Done():
		// 	sm.MustBeStopped = true
		// case <-sm.Done:
		//
		// }
		time.Sleep(stoppingTTL)

		if !sm.IsStopped() {
			continue
		}

		wg.Done()
		return
	}
}
