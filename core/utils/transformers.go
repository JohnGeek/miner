package utils

import (
	btcdchainhash "github.com/btcsuite/btcd/chaincfg/chainhash"
	btcdwire "github.com/btcsuite/btcd/wire"
	"gitlab.com/jaxnet/jaxnetd/types/chainhash"
	"gitlab.com/jaxnet/jaxnetd/types/wire"
)

func BtcBlockToBlockAux(btcBlock *btcdwire.MsgBlock) wire.BTCBlockAux {
	btcCoinbaseTx := btcBlock.Transactions[0].Copy()
	coinbaseTx := BtcTxToJaxTx(btcCoinbaseTx)
	txHashes := make([]chainhash.Hash, len(btcBlock.Transactions))

	for i, transaction := range btcBlock.Transactions {
		txHashes[i] = chainhash.Hash(transaction.TxHash())
	}

	return wire.BTCBlockAux{
		Version:    btcBlock.Header.Version,
		PrevBlock:  chainhash.Hash(btcBlock.Header.PrevBlock),
		MerkleRoot: chainhash.Hash(btcBlock.Header.MerkleRoot),
		Timestamp:  btcBlock.Header.Timestamp,
		Bits:       btcBlock.Header.Bits,
		Nonce:      btcBlock.Header.Nonce,
		CoinbaseAux: wire.CoinbaseAux{
			Tx:            coinbaseTx,
			TxMerkleProof: chainhash.BuildCoinbaseMerkleTreeProof(txHashes),
		},
	}
}

func BtcTxToJaxTx(tx *btcdwire.MsgTx) wire.MsgTx {
	// tx := btcBlock.Transactions[0].Copy()
	msgTx := wire.MsgTx{
		Version:  tx.Version,
		TxIn:     make([]*wire.TxIn, len(tx.TxIn)),
		TxOut:    make([]*wire.TxOut, len(tx.TxOut)),
		LockTime: tx.LockTime,
	}

	for i := range msgTx.TxIn {
		msgTx.TxIn[i] = &wire.TxIn{
			PreviousOutPoint: wire.OutPoint{
				Hash:  chainhash.Hash(tx.TxIn[i].PreviousOutPoint.Hash),
				Index: tx.TxIn[i].PreviousOutPoint.Index,
			},
			SignatureScript: tx.TxIn[i].SignatureScript,
			Witness:         wire.TxWitness(tx.TxIn[i].Witness),
			Sequence:        tx.TxIn[i].Sequence,
		}
	}

	for i := range msgTx.TxOut {
		msgTx.TxOut[i] = &wire.TxOut{
			Value:    tx.TxOut[i].Value,
			PkScript: tx.TxOut[i].PkScript,
		}
	}
	return msgTx
}

func JaxTxToBtcTx(tx *wire.MsgTx) btcdwire.MsgTx {
	msgTx := btcdwire.MsgTx{
		Version:  tx.Version,
		TxIn:     make([]*btcdwire.TxIn, len(tx.TxIn)),
		TxOut:    make([]*btcdwire.TxOut, len(tx.TxOut)),
		LockTime: tx.LockTime,
	}

	for i := range msgTx.TxIn {
		msgTx.TxIn[i] = &btcdwire.TxIn{
			PreviousOutPoint: btcdwire.OutPoint{
				Hash:  btcdchainhash.Hash(tx.TxIn[i].PreviousOutPoint.Hash),
				Index: tx.TxIn[i].PreviousOutPoint.Index,
			},
			SignatureScript: tx.TxIn[i].SignatureScript,
			Witness:         btcdwire.TxWitness(tx.TxIn[i].Witness),
			Sequence:        tx.TxIn[i].Sequence,
		}
	}

	for i := range msgTx.TxOut {
		msgTx.TxOut[i] = &btcdwire.TxOut{
			Value:    tx.TxOut[i].Value,
			PkScript: tx.TxOut[i].PkScript,
		}
	}
	return msgTx
}
