package utils

import (
	"math/big"

	"gitlab.com/jaxnet/jaxnetd/types/pow"
)

// TODO: unused function
func Bits2Difficulty(bits uint32) (float *big.Float) {
	target := pow.CompactToBig(bits)
	MaxTargetTruncated, _ := new(big.Int).SetString("00000000FFFF0000000000000000000000000000000000000000000000000000", 16)
	bigDiff := new(big.Float).Quo(
		new(big.Float).SetInt(MaxTargetTruncated),
		new(big.Float).SetInt(target),
	)
	return bigDiff
}

func Target2Difficulty(target *big.Int) (float *big.Float) {
	MaxTargetTruncated, _ := new(big.Int).SetString("00000000FFFF0000000000000000000000000000000000000000000000000000", 16)
	bigDiff := new(big.Float).Quo(
		new(big.Float).SetInt(MaxTargetTruncated),
		new(big.Float).SetInt(target),
	)
	return bigDiff
}

func Difficulty2Target(bigDiff *big.Float) *big.Int {
	MaxTargetTruncated, _ := new(big.Int).SetString("00000000FFFF0000000000000000000000000000000000000000000000000000", 16)
	targetFloat := new(big.Float).Quo(
		new(big.Float).SetInt(MaxTargetTruncated),
		bigDiff,
	)
	target, _ := targetFloat.Int(nil)

	return target
}
