package utils

import (
	"bytes"
	"crypto/sha256"
	"encoding/hex"
)

func CalculateSteps(data [][]byte) [][]byte {
	L := data
	steps := make([][]byte, 0)
	PreL := [][]byte{nil}
	StartL := 2
	Ll := len(L)

	for Ll > 1 {
		steps = append(steps, L[1])

		if Ll%2 != 0 {
			L = append(L, L[len(L)-1])
		}

		r := Range(StartL, Ll, 2)
		Ld := make([][]byte, len(r))

		for i := 0; i < len(r); i++ {
			Ld[i] = MerkleJoin(L[r[i]], L[r[i]+1])
		}
		L = append(PreL, Ld...)
		Ll = len(L)
	}

	return steps
}

func MerkleJoin(h1, h2 []byte) []byte {
	return Sha256d(bytes.Join([][]byte{h1, h2}, nil))
}

func GetMerkleHashes(steps [][]byte) []string {
	hashes := make([]string, len(steps))
	for i := 0; i < len(steps); i++ {
		// hash := make([]byte, 32)
		// copy(hash, steps[i])
		hashes[i] = hex.EncodeToString(steps[i])
	}

	return hashes
}

// range steps between [start, end)
func Range(start, stop, step int) []int {
	if (step > 0 && start >= stop) || (step < 0 && start <= stop) {
		return []int{}
	}

	result := make([]int, 0)
	i := start
	for {
		if step > 0 {
			if i < stop {
				result = append(result, i)
			} else {
				break
			}
		} else {
			if i > stop {
				result = append(result, i)
			} else {
				break
			}
		}
		i += step
	}

	return result
}

func Sha256(b []byte) []byte {
	b32 := sha256.Sum256(b)
	return b32[:]
}

func Sha256d(b []byte) []byte {
	return Sha256(Sha256(b))
}
