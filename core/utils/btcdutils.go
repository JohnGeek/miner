package utils

import (
	btcdchainhash "github.com/btcsuite/btcd/chaincfg/chainhash"
	btcdwire "github.com/btcsuite/btcd/wire"
)

// BitcoinBlockCopy creates a deep copy of a MsgBlock so that the original does not get
// modified when the copy is manipulated.
func BitcoinBlockCopy(msg *btcdwire.MsgBlock) *btcdwire.MsgBlock {
	clone := new(btcdwire.MsgBlock)
	clone.Header = msg.Header
	clone.Transactions = make([]*btcdwire.MsgTx, len(msg.Transactions))
	for i, tx := range msg.Transactions {
		clone.Transactions[i] = tx.Copy()
	}

	return clone
}

func MerkleTreeBranch(merkles []*btcdchainhash.Hash) (path []*btcdchainhash.Hash) {
	b := len(merkles)

	for k := b; k > 1; {
		path = append(path, merkles[b-k+1])
		k = (k - 1) / 2
	}

	return
}

func MerkleTreeBranchString(merkles []string) (path []string) {
	b := len(merkles)

	for k := b; k > 1; {
		path = append(path, merkles[b-k+1])
		k = (k - 1) / 2
	}

	return
}
