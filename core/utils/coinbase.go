package utils

import (
	"bytes"
	"fmt"

	"github.com/btcsuite/btcd/blockchain"
	btcdwire "github.com/btcsuite/btcd/wire"
	btcdutil "github.com/btcsuite/btcutil"
	"gitlab.com/jaxnet/core/miner/core/logger"
	"gitlab.com/jaxnet/jaxnetd/jaxutil"
	"gitlab.com/jaxnet/jaxnetd/node/chaindata"
	"gitlab.com/jaxnet/jaxnetd/txscript"
	"gitlab.com/jaxnet/jaxnetd/types/chainhash"
	"gitlab.com/jaxnet/jaxnetd/types/wire"
)

// CoinbaseFlags is added to the coinbase script of a generated block
// and is used to monitor BIP16 support as well as blocks that are
// generated via btcd.
const CoinbaseFlags = "/P2SH/jaxnet/"

// extraNonceLen is a length of extranonce value
const extraNonceLen = 8

// extraNonceSize is an amount of bytes needed to store extranonce size/len.
const extraNonceSize = 1

type BitcoinBlockCoinbaseParts struct {
	Head []byte
	Tail []byte
}

// JaxCoinbaseScript returns a standard script suitable for use as the
// signature script of the coinbase transaction of a new block.  In particular,
// it starts with the block height that is required by version 2 blocks and adds
// the extra nonce as well as additional coinbase flags.
func JaxCoinbaseScript(nextBlockHeight int64, chainID uint32, extraNonce uint64) ([]byte, error) {
	return txscript.NewScriptBuilder().
		AddInt64(nextBlockHeight).
		AddInt64(int64(extraNonce)).
		AddInt64(int64(chainID)).
		AddData([]byte(CoinbaseFlags)).
		Script()
}

func SetStratumExtraNonce(block btcdwire.MsgBlock, coinbaseParts BitcoinBlockCoinbaseParts, extraNonce []byte) btcdwire.MsgBlock {
	logger.Log.Trace().Int("len", len(extraNonce)).Int("coinbaseScriptLen", len(coinbaseParts.Head)+extraNonceSize+extraNonceLen+len(coinbaseParts.Tail)).Msg("Debug extraNonce")
	coinbaseScript := make([]byte, 0, len(coinbaseParts.Head)+extraNonceSize+extraNonceLen+len(coinbaseParts.Tail))
	coinbaseScript = append(coinbaseParts.Head, byte(len(extraNonce)))
	coinbaseScript = append(coinbaseScript, extraNonce...)
	coinbaseScript = append(coinbaseScript, coinbaseParts.Tail...)

	block.Transactions[0].TxIn[0].SignatureScript = coinbaseScript

	// Recalculate the merkle root with the updated extra nonce.
	newBlock := btcdutil.NewBlock(&block)
	merkles := blockchain.BuildMerkleTreeStore(newBlock.Transactions(), false)
	block.Header.MerkleRoot = *merkles[len(merkles)-1]

	return block
}

func UpdateBitcoinExtraNonce(block btcdwire.MsgBlock, height int64, extraNonce uint64, beaconHash []byte) (btcdwire.MsgBlock, error) {
	coinbaseScript, err := chaindata.BTCCoinbaseScript(height, PackUint64LE(extraNonce), beaconHash)
	if err != nil {
		return btcdwire.MsgBlock{}, err
	}
	if len(coinbaseScript) > chaindata.MaxCoinbaseScriptLen {
		return btcdwire.MsgBlock{}, fmt.Errorf("coinbase transaction script length of %d is out of range (min: %d, max: %d)",
			len(coinbaseScript), chaindata.MinCoinbaseScriptLen, chaindata.MaxCoinbaseScriptLen)
	}

	block.Transactions[0].TxIn[0].SignatureScript = coinbaseScript

	// Recalculate the merkle root with the updated extra nonce.
	newBlock := btcdutil.NewBlock(&block)
	merkles := blockchain.BuildMerkleTreeStore(newBlock.Transactions(), false)
	block.Header.MerkleRoot = *merkles[len(merkles)-1]

	return block, nil
}

func UpdateBeaconExtraNonce(beaconBlock wire.MsgBlock, height int64, extraNonce uint64) (wire.MsgBlock, error) {
	bh := beaconBlock.Header.BeaconHeader().BeaconExclusiveHash()
	coinbaseScript, err := chaindata.BTCCoinbaseScript(height, PackUint64LE(extraNonce), bh[:])

	if err != nil {
		return wire.MsgBlock{}, err
	}
	if len(coinbaseScript) > chaindata.MaxCoinbaseScriptLen {
		return wire.MsgBlock{}, fmt.Errorf("coinbase transaction script length of %d is out of range (min: %d, max: %d)",
			len(coinbaseScript), chaindata.MinCoinbaseScriptLen, chaindata.MaxCoinbaseScriptLen)
	}
	beaconBlock.Header.UpdateCoinbaseScript(coinbaseScript)
	UpdateMerkleRoot(&beaconBlock)

	return beaconBlock, nil
}

func UpdateMerkleRoot(msgBlock *wire.MsgBlock) {
	root := msgBlock.Header.MerkleRoot()
	if root.IsEqual(&chainhash.ZeroHash) {
		// Recalculate the merkle root with the updated extra nonce.
		block := jaxutil.NewBlock(msgBlock)
		merkles := chaindata.BuildMerkleTreeStore(block.Transactions(), false)
		msgBlock.Header.SetMerkleRoot(*merkles[len(merkles)-1])
	}
}

func UpdateShardsMerkleRoot(shardBlock wire.MsgBlock, height int64, shardID uint32,
	extraNonce uint64) (wire.MsgBlock, error) {
	coinbaseScript, err := JaxCoinbaseScript(height, shardID, extraNonce)
	if err != nil {
		return wire.MsgBlock{}, err
	}
	if len(coinbaseScript) > chaindata.MaxCoinbaseScriptLen {
		return wire.MsgBlock{}, fmt.Errorf("coinbase transaction script length of %d is out of range (min: %d, max: %d)",
			len(coinbaseScript), chaindata.MinCoinbaseScriptLen, chaindata.MaxCoinbaseScriptLen)
	}

	// Recalculate the merkle root with the updated extra nonce.
	shardBlock.Header.UpdateCoinbaseScript(coinbaseScript)

	UpdateMerkleRoot(&shardBlock)

	return shardBlock, nil
}

func SplitCoinbase(block btcdwire.MsgBlock) ([]byte, []byte) {
	buf := bytes.NewBuffer(nil)
	block.Transactions[0].Serialize(buf)

	rawTx := buf.Bytes()
	heightLenIdx := 42
	heightLen := int(rawTx[heightLenIdx])
	if heightLen > 0xF {
		// if value more than 0xF,
		// this indicates that height is packed as an opcode OP_0 .. OP_16 (small int)
		// height = int(rawTx[heightLenIdx] - (txscript.OP_1 - 1))
		// so height value doesn't gives additional padding
		heightLen = 0
	}

	extraNonceLenIdx := heightLenIdx + 1 + heightLen
	extraNonceLen := int(rawTx[extraNonceLenIdx])
	// extraNonceLenIdx := 42
	// extraNonceLen := int(rawTx[extraNonceLenIdx])

	part1 := rawTx[0:extraNonceLenIdx]
	part1 = append(part1, 0x08)
	part2 := rawTx[extraNonceLenIdx+extraNonceLen+1:]
	return part1, part2
}
