package utils

import (
	"bytes"
	"encoding/binary"
	"encoding/hex"
	"math/rand"
	"strings"

	"github.com/pkg/errors"
)

func StringsIndexOf(data []string, element string) int {
	for k, v := range data {
		if strings.Compare(element, v) == 0 {
			return k
		}
	}
	return -1 // not found.
}

func RandHexUint64() string {
	randomNumBytes := make([]byte, 8)
	// this method never returns an error.
	_, _ = rand.Read(randomNumBytes)

	return hex.EncodeToString(randomNumBytes)
}

// ReverseByteOrder converts bytes in Little endian from/to Big endian
func ReverseByteOrder(b []byte) []byte {
	_b := make([]byte, len(b))
	copy(_b, b)

	for i := 0; i < 8; i++ {
		binary.LittleEndian.PutUint32(_b[i*4:], binary.BigEndian.Uint32(_b[i*4:]))
	}
	return ReverseBytes(_b)
}

func ReverseBytes(b []byte) []byte {
	_b := make([]byte, len(b))
	copy(_b, b)

	for i, j := 0, len(_b)-1; i < j; i, j = i+1, j-1 {
		_b[i], _b[j] = _b[j], _b[i]
	}
	return _b
}

func PackInt32BE(n int32) []byte {
	b := make([]byte, 4)
	binary.BigEndian.PutUint32(b, uint32(n))
	return b
}

func PackInt32LE(n int32) []byte {
	b := make([]byte, 4)
	binary.LittleEndian.PutUint32(b, uint32(n))
	return b
}

func SerializeNumber(n uint64) []byte {
	if n >= 1 && n <= 16 {
		return []byte{
			0x50 + byte(n),
		}
	}

	l := 1
	buff := make([]byte, 9)
	for n > 0x7f {
		buff[l] = byte(n & 0xff)
		l++
		n >>= 8
	}
	buff[0] = byte(l)
	buff[l] = byte(n)

	return buff[0 : l+1]
}

func SerializeString(s string) []byte {
	if len(s) < 253 {
		return bytes.Join([][]byte{
			{byte(len(s))},
			[]byte(s),
		}, nil)
	} else if len(s) < 0x10000 {
		return bytes.Join([][]byte{
			{253},
			PackUint16LE(uint16(len(s))),
			[]byte(s),
		}, nil)
	} else if int64(len(s)) < int64(0x100000000) {
		return bytes.Join([][]byte{
			{254},
			PackUint32LE(uint32(len(s))),
			[]byte(s),
		}, nil)
	} else {
		return bytes.Join([][]byte{
			{255},
			PackUint64LE(uint64(len(s))),
			[]byte(s),
		}, nil)
	}
}

func PackUint16LE(n uint16) []byte {
	b := make([]byte, 2)
	binary.LittleEndian.PutUint16(b, n)
	return b
}

func PackUint32BE(n uint32) []byte {
	b := make([]byte, 4)
	binary.BigEndian.PutUint32(b, n)
	return b
}

func PackUint32LE(n uint32) []byte {
	b := make([]byte, 4)
	binary.LittleEndian.PutUint32(b, n)
	return b
}

func PackUint64LE(n uint64) []byte {
	b := make([]byte, 8)
	binary.LittleEndian.PutUint64(b, n)
	return b
}

func VarIntBytes(n uint64) []byte {
	if n < 0xFD {
		return []byte{byte(n)}
	}

	if n <= 0xFFFF {
		buff := make([]byte, 3)
		buff[0] = 0xFD
		binary.LittleEndian.PutUint16(buff[1:], uint16(n))
		return buff
	}

	if n <= 0xFFFFFFFF {
		buff := make([]byte, 5)
		buff[0] = 0xFE
		binary.LittleEndian.PutUint32(buff[1:], uint32(n))
		return buff
	}

	buff := make([]byte, 9)
	buff[0] = 0xFF
	binary.LittleEndian.PutUint64(buff[1:], n)
	return buff
}

func Uint256BytesFromHash(h string) ([]byte, error) {
	container := make([]byte, 32)
	fromHex, err := hex.DecodeString(h)
	if err != nil {
		return nil, errors.Wrap(err, "unable to decode uint256")
	}

	copy(container, fromHex)

	return ReverseBytes(container), nil
}

func RandUint64() uint64 {
	buf := make([]byte, 8)
	_, _ = rand.Read(buf) // Always succeeds, no need to check error
	return binary.LittleEndian.Uint64(buf)
}

func RandUint32() uint32 {
	buf := make([]byte, 4)
	_, _ = rand.Read(buf) // Always succeeds, no need to check error
	return binary.LittleEndian.Uint32(buf)
}
