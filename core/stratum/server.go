package stratum

import (
	"net"
	"sync"
	"time"

	"gitlab.com/jaxnet/core/miner/core/communicator/events"
	"gitlab.com/jaxnet/core/miner/core/logger"
	"gitlab.com/jaxnet/core/miner/core/metrics"
	"gitlab.com/jaxnet/core/miner/core/miner/tasks"
	"gitlab.com/jaxnet/core/miner/core/settings"
	"gitlab.com/jaxnet/core/miner/core/state"
	"gitlab.com/jaxnet/core/miner/core/stratum/bans"
	"gitlab.com/jaxnet/core/miner/core/stratum/types"
	"gitlab.com/jaxnet/core/miner/core/stratum/vardiff"
	"gitlab.com/jaxnet/core/miner/core/utils"
)

// workerSubmittedShares describes data structure to store count of submitted shares from worker
type workerSubmittedShares struct {
	ValidShares   uint
	InvalidShares uint
}

var (
	workerSubmittedSharesTotal      map[string]workerSubmittedShares
	workerSubmittedSharesLastMinute map[string]workerSubmittedShares
	workerSubmittedSharesMutex      sync.Mutex

	workersActivity      map[string]workerActivity
	workersActivityMutex sync.Mutex
)

// Server describes structure of the stratum server.
type Server struct {
	utils.StoppableMixin
	stopped             bool
	Config              *settings.Configuration
	Listener            net.Listener
	VarDiff             *vardiff.VarDiff
	StratumClients      map[uint64]*Client
	StratumClientsMutex sync.Mutex
	SubscriptionCounter *SubscriptionCounter
	BanningManager      *bans.BanningManager
	JobManager          *JobManager
	// rebroadcastTicker     *time.Ticker
	ReceiveMiningJobEvent       chan struct{}
	APIServer                   *API
	ReceiveWorkerSubmittedShare chan WorkerSubmittedShare
	receiveWorkerActivityEvent  chan workerActivity
	StratumUpdateBlockEvent     chan events.StratumUpdateBlock
}

func NewStratumServer(options *settings.Configuration) *Server {
	InitClientCtxPool()

	workerSubmittedSharesTotal = make(map[string]workerSubmittedShares)
	workerSubmittedSharesLastMinute = make(map[string]workerSubmittedShares)
	workersActivity = make(map[string]workerActivity)

	return &Server{
		Config:                      options,
		BanningManager:              bans.NewBanningManager(options),
		SubscriptionCounter:         NewSubscriptionCounter(),
		StratumClients:              make(map[uint64]*Client),
		ReceiveMiningJobEvent:       make(chan struct{}),
		APIServer:                   NewAPIServer(options.Stratum.APIServer),
		ReceiveWorkerSubmittedShare: make(chan WorkerSubmittedShare),
		receiveWorkerActivityEvent:  make(chan workerActivity),
	}
}

func (ss *Server) Init(minerResultChan chan tasks.MinerResult, stratumUpdateBlockEvent chan events.StratumUpdateBlock) {
	if ss.Config.Stratum.Banning.Enabled {
		ss.BanningManager.Init()
	}

	ss.JobManager = NewJobManager(ss.Config, minerResultChan)

	// Get channel from communicator and pass to stratum client to handle mining.update_block method
	ss.StratumUpdateBlockEvent = stratumUpdateBlockEvent

	var err error
	if ss.Config.Stratum.Ports.TLS {
		// TODO: fix
		// ss.Listener, err = tls.Listen("tcp", ":"+strconv.Itoa(port), options.TLS.ToTLSConfig())
	} else {
		// ss.Listener, err = net.Listen("tcp", ":"+strconv.Itoa(port))
		ss.Listener, err = net.Listen("tcp4", ":"+ss.Config.Stratum.Ports.Port)
	}

	if err != nil {
		logger.Log.Panic().Err(err).Msg("Failed to start stratum server")
	} else {
		logger.Log.Info().Str("port", ss.Config.Stratum.Ports.Port).Msg("Stratum server started")
	}

	// API server
	go func() {
		ss.APIServer.Run()
	}()

	// Job fetching
	go func() {
		for {
			<-ss.ReceiveMiningJobEvent
			logger.Log.Trace().Msg("ss.ReceiveMiningJobEvent")
			currentJob := ss.JobManager.CurrentJob
			if currentJob.JobId != "" && !currentJob.Broadcasted {
				logger.Log.Info().Str("currentJobId", currentJob.JobId).Msg("currentJobId")

				jobParams, err := ss.JobManager.GetJobParamsEx(ss.JobManager.CurrentJob, false)
				if err != nil {
					logger.Log.Error().Err(err).Msg("Failed to get JobParamsEx")
					continue
				}

				go ss.BroadcastCurrentMiningJob(jobParams)
				currentJob.Broadcasted = true

				metrics.IncCounterVec(metrics.SubmittedJobs, nil)
			} else {
				logger.Log.Trace().Interface("current Job", currentJob).Msg("debug current job")
			}
		}
	}()

	// New clients handling
	go func() {
		for {
			conn, err := ss.Listener.Accept()
			if err != nil {
				logger.Log.Error().Err(err).Msg("Failed to accept request")
				continue
			}

			if conn != nil {
				logger.Log.Info().Str("from", conn.RemoteAddr().String()).Msg("New miner conn")

				// Increase connection deadline on each subsequent message
				deadline := time.Now().Add(time.Duration(ss.Config.Stratum.ConnectionTimeout) * time.Second)
				err = conn.SetDeadline(deadline)
				if err != nil {
					logger.Log.Error().Err(err).Str("from", conn.RemoteAddr().String()).Msg("Failed to increase connection deadline")
					continue
				} else {
					logger.Log.Trace().Str("from", conn.RemoteAddr().String()).Int64("deadline", deadline.Unix()).Msg("Increased connection deadline")
				}

				go ss.HandleNewClient(conn)
			}
		}
	}()

	// Submitted shares by worker
	go func() {
		statsTicker := time.NewTicker(1 * time.Minute)
		logger.Log.Info().Msg("statsTicker started")
		defer logger.Log.Info().Msg("statsTicker stopped")
		defer statsTicker.Stop()

		for {
			select {
			case res := <-ss.ReceiveWorkerSubmittedShare:
				logger.Log.Info().Msg("received worker share for statistics")
				workerSubmittedSharesMutex.Lock()
				total := workerSubmittedSharesTotal[res.Worker]
				lastMinute := workerSubmittedSharesLastMinute[res.Worker]
				if res.ValidShare {
					total.ValidShares++
					lastMinute.ValidShares++
				} else {
					total.InvalidShares++
					lastMinute.InvalidShares++
				}
				workerSubmittedSharesTotal[res.Worker] = total
				workerSubmittedSharesLastMinute[res.Worker] = lastMinute
				workerSubmittedSharesMutex.Unlock()
			case <-statsTicker.C:
				logger.Log.Trace().Msg("received stats ticker event")
				workerSubmittedSharesMutex.Lock()
				logger.Log.Info().Interface("lastMinute", workerSubmittedSharesLastMinute).Interface("total", workerSubmittedSharesTotal).Msg("submitted shares by workers")

				for worker := range workerSubmittedSharesLastMinute {
					metrics.SetGaugeVec(metrics.SubmittedShares, map[string]string{"worker": worker, "valid_shares": "true"},
						float64(workerSubmittedSharesLastMinute[worker].ValidShares),
					)

					metrics.SetGaugeVec(metrics.SubmittedShares, map[string]string{"worker": worker, "valid_shares": "false"},
						float64(workerSubmittedSharesLastMinute[worker].InvalidShares),
					)

					workerSubmittedSharesLastMinute[worker] = workerSubmittedShares{}
				}

				// workerSubmittedSharesLastMinute = make(map[string]workerSubmittedShares)
				workerSubmittedSharesMutex.Unlock()
			}
		}
	}()

	// worker activity
	go func() {
		for {
			res := <-ss.receiveWorkerActivityEvent
			// logger.Log.Trace().Msg("Received worker connected event from client for statistics")
			workersActivityMutex.Lock()
			rec, ok := workersActivity[res.Worker]
			if ok {
				rec.Reconnected = res.Connected
			} else {
				workersActivity[res.Worker] = workerActivity{
					Worker:    res.Worker,
					Connected: res.Connected,
				}
			}
			workersActivityMutex.Unlock()
		}
	}()

}

// HandleNewClient converts the conn to an underlying client instance and finally return its unique subscriptionID
func (ss *Server) HandleNewClient(socket net.Conn) uint64 {
	ss.StratumClientsMutex.Lock()
	subscriptionID := ss.SubscriptionCounter.Next()
	client := NewStratumClient(subscriptionID,
		socket, ss.Config, ss.BanningManager, ss.JobManager,
		ss.ReceiveMiningJobEvent, ss.ReceiveWorkerSubmittedShare,
		ss.receiveWorkerActivityEvent, ss.StratumUpdateBlockEvent)

	ss.StratumClients[subscriptionID] = client
	ss.StratumClientsMutex.Unlock()
	// client.connected

	go func() {
		for {
			logger.Log.Trace().Uint64("subscriptionID", subscriptionID).Msg("goroutine HandleNewClient started")
			<-client.SocketClosedEvent
			logger.Log.Trace().Uint64("subscriptionID", subscriptionID).Msg("received sc.SocketClosedEvent in server")
			err := client.Socket.Close()
			if err != nil {
				logger.Log.Trace().Err(err).Uint64("subscriptionID", subscriptionID).Msg("failed to close client connection")

			}
			logger.Log.Trace().Uint64("subscriptionID", subscriptionID).Msg("closed client connection")

			client.VarDiffStop()
			ss.RemoveStratumClientBySubscriptionId(subscriptionID)
			// client.disconnected
			break
		}
	}()

	client.Init()

	return subscriptionID
}

func (ss *Server) BroadcastCurrentMiningJob(jobParams types.JobParams) {
	logger.Log.Trace().Msg("broadcasting job params")
	for clientId := range ss.StratumClients {
		ss.StratumClients[clientId].SendMiningJob(jobParams)
	}
}

func (ss *Server) RemoveStratumClientBySubscriptionId(subscriptionId uint64) {
	ss.StratumClientsMutex.Lock()
	delete(ss.StratumClients, subscriptionId)
	ss.StratumClientsMutex.Unlock()
}

func (ss *Server) ManuallyAddStratumClient(client *Client) {
	subscriptionId := ss.HandleNewClient(client.Socket)
	if subscriptionId != 0 {
		ss.StratumClients[subscriptionId].ManuallyAuthClient(client.WorkerName, client.WorkerPass)
		ss.StratumClients[subscriptionId].ManuallySetValues(client)
	}
}

// Run sends current mining task to miner.
func (ss *Server) Run(c *state.Coordinator) {
	var (
		currentTask *tasks.MinerTask
		nextTask    *tasks.MinerTask
	)

	for {
		if ss.MustBeStopped {
			ss.stopped = true
			return
		}

		nextTask = c.NextTask()
		if nextTask == nil && currentTask == nil {
			// There is no work available for miner.
			// Even if current task is not nil - no mining must be done,
			// because the actual BC header has been set to nil on the ctx's side.
			//
			// Wait some time and check again later.
			time.Sleep(tasks.MiningRoundTimeWindow)
			continue
		}

		if currentTask == nil || currentTask != nextTask {
			// The task has been updated, so the mining context must be reset.
			// In all other cases the mining would be continued with previous context.

			// Possible optimisation when building stratum job.
			// Wait until bitcoin, beacon and shard block candidates are fetched
			// logger.Log.Info().Uint8("block flags", uint8(nextTask.BlockFlags)).Int("shards number", len(nextTask.ShardsTargets)).Msg("debug nextTask")
			// if (nextTask.BlockFlags&(tasks.BitcoinFlag|tasks.BeaconFlag) != 0) && len(nextTask.ShardsTargets) > 0 {
			// if nextTask.BlockFlags&(tasks.BitcoinFlag|tasks.BeaconFlag) == nextTask.BlockFlags && len(nextTask.ShardsTargets) == 3 {
			if nextTask.BlockFlags&(tasks.BitcoinFlag|tasks.BeaconFlag) == nextTask.BlockFlags {
				ss.JobManager.CurrentJob = *NewJob(nextTask)

				// TODO: not sure if optimisation below is correct in prod env
				currentTask = nextTask
				// logger.Log.Info().Str("currentJobId", ss.JobManager.CurrentJob.JobId).Msg("currentJobId")
				// logger.Log.Info().Str("currentJob", spew.Sprintf("%s", ss.JobManager.CurrentJob)).Msg("current job dump")
				// TODO: maybe send task via channel only, not just a notification about new task?
				// time.Sleep(tasks.MiningRoundTimeWindow)
				ss.ReceiveMiningJobEvent <- struct{}{}
			} else {
				logger.Log.Info().Uint8("block flags", uint8(nextTask.BlockFlags)).Int("shards number", len(nextTask.ShardsTargets)).Msg("nextTask is not fully populated with data")
				time.Sleep(tasks.MiningRoundTimeWindow)
			}
		}
	}
}
