package stratum

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"math/big"
	"net"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/google/uuid"
	"github.com/patrickmn/go-cache"
	"gitlab.com/jaxnet/core/miner/core/communicator/events"
	"gitlab.com/jaxnet/core/miner/core/logger"
	"gitlab.com/jaxnet/core/miner/core/metrics"
	"gitlab.com/jaxnet/core/miner/core/settings"
	"gitlab.com/jaxnet/core/miner/core/stratum/bans"
	"gitlab.com/jaxnet/core/miner/core/stratum/types"
	"gitlab.com/jaxnet/core/miner/core/stratum/vardiff"
	"gitlab.com/jaxnet/core/miner/core/utils"
	"gitlab.com/jaxnet/jaxnetd/types/pow"
)

var clientCtxPool *cache.Cache

// 10 minutes and 1 minute
const avgBitcoinBlockMiningIntervalExpiration = 10 + 1

// Purge 5 minutes after expiration
const avgBitcoinBlockMiningIntervalPurge = avgBitcoinBlockMiningIntervalExpiration + 5

// Version rolling constants
const versionRollingServerMask = uint32(0x1fffe000)

// Max message size/buffer size in bytes
const maxMessageSize = 4 * 1024

// Minimum pool difficulty, subject to change sometime
const minPoolDifficulty = 0.001

func InitClientCtxPool() {
	clientCtxPool = cache.New(avgBitcoinBlockMiningIntervalExpiration*time.Minute, avgBitcoinBlockMiningIntervalPurge*time.Minute)
}

// WorkerSubmittedShare represents data for realtime worker statistics for submitted shares
type WorkerSubmittedShare struct {
	Worker     string
	ValidShare bool
}

// workerActivity represents data to track worker (re)connects
type workerActivity struct {
	Worker      string
	Connected   int64
	Reconnected int64
}

type Client struct {
	SubscriptionId uint64
	Options        *settings.Configuration
	RemoteAddress  net.Addr

	Socket           net.Conn
	SocketBufIO      *bufio.ReadWriter
	SocketBufIOMutex sync.Mutex

	LastActivity time.Time
	Shares       *Shares

	IsAuthorized           bool
	SubscriptionBeforeAuth bool

	ExtraNonce1 uint32

	varDiff *vardiff.VarDiff

	WorkerName string
	WorkerPass string

	// ToDo: Seems like may be removed
	PendingTarget  uint32
	CurrentTarget  uint32
	PreviousTarget uint32

	PendingDifficulty  *big.Float
	CurrentDifficulty  *big.Float
	PreviousDifficulty *big.Float
	ClientTarget       *big.Int

	JobManager                  *JobManager
	BanningManager              *bans.BanningManager
	SocketClosedEvent           chan struct{}
	SendMiningJobEvent          chan struct{}
	SendWorkerSubmittedShare    chan WorkerSubmittedShare
	sendWorkerActivityEvent     chan workerActivity
	sendStratumUpdateBlockEvent chan events.StratumUpdateBlock

	// Version rolling support
	versionRolling types.VersionRolling
}

func NewStratumClient(subscriptionId uint64, socket net.Conn, options *settings.Configuration, bm *bans.BanningManager, jm *JobManager,
	sendMiningJobEvent chan struct{}, sendWorkerSubmittedShare chan WorkerSubmittedShare, sendWorkerActivityEvent chan workerActivity, stratumUpdateBlock chan events.StratumUpdateBlock) *Client {

	// Init variable difficulty
	varDiff := vardiff.NewVarDiff(options)

	return &Client{
		SubscriptionId:              subscriptionId,
		Options:                     options,
		RemoteAddress:               socket.RemoteAddr(),
		Socket:                      socket,
		SocketBufIO:                 bufio.NewReadWriter(bufio.NewReaderSize(socket, maxMessageSize), bufio.NewWriterSize(socket, maxMessageSize)),
		LastActivity:                time.Now(),
		IsAuthorized:                false,
		SubscriptionBeforeAuth:      false,
		varDiff:                     varDiff,
		BanningManager:              bm,
		JobManager:                  jm,
		Shares:                      new(Shares),
		ExtraNonce1:                 utils.RandUint32(),
		SocketClosedEvent:           make(chan struct{}),
		SendMiningJobEvent:          sendMiningJobEvent,
		SendWorkerSubmittedShare:    sendWorkerSubmittedShare,
		sendWorkerActivityEvent:     sendWorkerActivityEvent,
		sendStratumUpdateBlockEvent: stratumUpdateBlock,
	}
}

func (sc *Client) Init() {
	sc.SetupSocket()
	sc.VarDiffStart()
}

func (sc *Client) SetupSocket() {
	sc.BanningManager.CheckBan(sc.RemoteAddress.String())
	once := true

	go func() {
		for {
			select {
			default:
				raw, err := sc.SocketBufIO.ReadBytes('\n')
				if err != nil {
					from := sc.GetLabel()

					if err == io.EOF {
						logger.Log.Trace().Str("from", from).Msg("received EOF, closing connection")

						sc.SocketClosedEvent <- struct{}{}
						return
					}

					e, ok := err.(net.Error)

					if !ok {
						logger.Log.Error().Err(err).Str("from", from).Msg("failed to read bytes from socket due to non-network error")
						return
					}

					if ok && e.Timeout() {
						logger.Log.Info().Err(err).Str("from", from).Msg("socket is timeout")
						sc.SocketClosedEvent <- struct{}{}
						return
					}

					if ok && e.Temporary() {
						logger.Log.Error().Err(err).Str("from", from).Msg("failed to read bytes from socket due to temporary error")
						continue
					}

					logger.Log.Error().Err(err).Str("from", from).Msg("failed to read bytes from socket")
					sc.SocketClosedEvent <- struct{}{}
					return
				}

				// Check for message flood
				if len(raw) > maxMessageSize {
					logger.Log.Warn().
						Str("from", sc.GetLabel()).
						Str("raw_data", string(raw)).
						Int("data_size", len(raw)).
						Int("max_data_size", maxMessageSize).
						Msg("Flooding message")
					sc.SocketClosedEvent <- struct{}{}
					return
				}

				logger.Log.Trace().Str("data", string(raw)).Int("data_size", len(raw)).Msg("Received new message")

				if len(raw) == 0 {
					continue
				}

				messageValid := true
				var message types.RPCRequest
				var loginMessage types.RPCLoginMessage
				err = json.Unmarshal(raw, &message)

				if err != nil {
					messageValid = false
					err = json.Unmarshal(raw, &loginMessage)

				}

				if err != nil {
					if !sc.Options.Stratum.TcpProxyProtocol {
						logger.Log.Error().Str("label", sc.GetLabel()).Str("data", string(raw)).Msg("Malformed message from")
						sc.SocketClosedEvent <- struct{}{}
					}

					return
				}

				if once && sc.Options.Stratum.TcpProxyProtocol {
					once = false
					if bytes.HasPrefix(raw, []byte("PROXY")) {
						sc.RemoteAddress, err = net.ResolveTCPAddr("tcp", string(bytes.Split(raw, []byte(" "))[2]))
						if err != nil {
							logger.Log.Error().Err(err).Msg("failed to resolve tcp addr behind proxy")
						}
					} else {
						logger.Log.Error().Str("raw data", string(raw)).
							Msg("Client IP detection failed, tcpProxyProtocol is enabled yet did not receive proxy protocol message, instead got data")
					}
				}
				// todo: handle banning
				sc.BanningManager.CheckBan(sc.RemoteAddress.String())

				if messageValid {
					sc.HandleMessage(&message)
				} else {
					sc.HandleLogin(&loginMessage, true)
				}
			}
		}
	}()
}

func (sc *Client) HandleMessage(message *types.RPCRequest) {
	switch message.Method {
	case types.Subscribe:
		sc.HandleSubscribe(message)
	case types.Authorize:
		sc.HandleAuthorize(message, true)
	case types.Submit:
		sc.LastActivity = time.Now()
		sc.HandleSubmit(message)
	case types.ExtranonceSubscribe:
		sc.SendJSON(types.AuthResponse(message.ID, true))
	case types.UpdateBlock:
		sc.HandleUpdateBlock(message)
	case types.Configure:
		sc.HandleConfigure(message)
	default:
		logger.Log.Warn().Interface("message", message).Msg("unknown stratum method")
	}
}

func (sc *Client) HandleSubscribe(message *types.RPCRequest) {
	logger.Log.Info().Msg("handling subscribe")
	if !sc.IsAuthorized {
		sc.SubscriptionBeforeAuth = true
	}

	var resumePrevConnectionCtx bool

	if len(message.Params) > 1 {
		logger.Log.Info().Msg("Miner wants to resume context before reconnection")

		// Miner wants to resume context before reconnection
		idStr := string(message.Params[1])
		idStr = strings.TrimPrefix(idStr, `"`)
		idStr = strings.TrimSuffix(idStr, `"`)

		id, err := strconv.ParseInt(idStr, 16, 64)
		if err != nil {
			logger.Log.Error().Err(err).Str("idStr", idStr).Msg("unable to parse connection id")
		} else {
			val, found := clientCtxPool.Get(fmt.Sprintf("%d", id))
			if found {
				resumePrevConnectionCtx = true
				sc.SubscriptionId = uint64(id)
				sc.ExtraNonce1 = val.(uint32)
			}
		}
	}

	if !resumePrevConnectionCtx {
		logger.Log.Info().Msg("Unable to resume previous connection context, new connection established")
		// New connection
		clientCtxPool.Set(fmt.Sprintf("%d", sc.SubscriptionId), sc.ExtraNonce1, cache.DefaultExpiration)
	}

	sc.SendJSON(types.SubscribeResponse(message.ID, sc.SubscriptionId, sc.ExtraNonce1))
}

func (sc *Client) HandleLogin(message *types.RPCLoginMessage, replyToSocket bool) {
	loginData := message.Params

	logger.Log.Info().Str("login", loginData.Login).Str("pass", loginData.Pass).
		Str("agent", loginData.Agent).Msg("Handling login")

	sc.WorkerName = loginData.Login
	sc.WorkerName = strings.TrimPrefix(sc.WorkerName, `"`)
	sc.WorkerName = strings.TrimSuffix(sc.WorkerName, `"`)

	sc.WorkerPass = loginData.Pass
	sc.WorkerPass = strings.TrimPrefix(sc.WorkerPass, `"`)
	sc.WorkerPass = strings.TrimSuffix(sc.WorkerPass, `"`)

	err := sc.Authorize()

	if replyToSocket {
		if sc.IsAuthorized {
			sc.SendJSON(types.AuthResponse(message.ID, sc.IsAuthorized))
		} else {
			sc.SendJSON(types.ErrorResponse(message.ID, 20, string(types.ToRawJSON(err))))
		}
	}

	if err != nil {
		logger.Log.Warn().Err(err).Str("worker", sc.WorkerName).Msg("Closed socket due to failed to authorize the miner")
		sc.SocketClosedEvent <- struct{}{}
		return
	}

	if sc.JobManager.CurrentJob.JobId == "" {
		time.Sleep(10 * time.Second) // TODO: Move this to config
		return
	}

	sc.InitPoolDifficulty()
	sc.SendMiningJobEvent <- struct{}{}
}

// VarDiffUpdate changes variable difficulty options for client connection on the fly.
func (sc *Client) VarDiffUpdate(clientOptions map[string]float64) {
	if !sc.Options.Stratum.Ports.VarDiff.Enabled {
		if value, ok := clientOptions["d"]; ok {
			sc.CurrentDifficulty = big.NewFloat(value)
			sc.PendingDifficulty = big.NewFloat(0)
			logger.Log.Trace().Float64("fixed diff", value).Msg("debug VarDiffUpdate")
		}
		return
	}

	for key, value := range clientOptions {
		switch key {
		case "d":
			sc.varDiff.FixDiff = value
		case "dstart":
			sc.varDiff.StartDiff = value
		case "dmin":
			sc.varDiff.MinDiff = value
		case "dmax":
			sc.varDiff.MaxDiff = value
		case "starget":
			sc.varDiff.TargetSharesPerTimeWindow = uint32(value)
		case "timewindow":
			sc.varDiff.TimeWindow = int64(value)
		case "retargettimewindow":
			sc.varDiff.RetargetTimeWindow = int64(value)
		case "jobpriority":
			sc.varDiff.JobPriority = value != 0
		default:
		}
	}
}

func (sc *Client) Authorize() error {
	authorized, clientOptions, err :=
		AuthorizeFn(sc.RemoteAddress, sc.Socket.LocalAddr().(*net.TCPAddr).Port, sc.WorkerName, sc.WorkerPass)
	sc.IsAuthorized = err == nil && authorized

	if err != nil {
		return err
	}

	// Update options
	sc.VarDiffUpdate(clientOptions)
	logger.Log.Trace().Str("worker_name", sc.WorkerName).Bool("is_authorized", sc.IsAuthorized).Msg("VarDiff updated")

	// Send data when worker connected
	sc.sendWorkerActivityEvent <- workerActivity{
		Worker:    sc.WorkerName,
		Connected: time.Now().UnixNano(),
	}

	logger.Log.Info().Str("worker_name", sc.WorkerName).Bool("is_authorized", sc.IsAuthorized).Msg("Worker authorized")

	return nil
}

func (sc *Client) HandleAuthorize(message *types.RPCRequest, replyToSocket bool) {
	logger.Log.Info().Msg("handling authorize")

	sc.WorkerName = string(message.Params[0])
	sc.WorkerName = strings.TrimPrefix(sc.WorkerName, `"`)
	sc.WorkerName = strings.TrimSuffix(sc.WorkerName, `"`)

	sc.WorkerPass = string(message.Params[1])
	sc.WorkerPass = strings.TrimPrefix(sc.WorkerPass, `"`)
	sc.WorkerPass = strings.TrimSuffix(sc.WorkerPass, `"`)

	err := sc.Authorize()

	if replyToSocket {
		if sc.IsAuthorized {
			sc.SendJSON(types.AuthResponse(message.ID, sc.IsAuthorized))
		} else {
			sc.SendJSON(types.ErrorResponse(message.ID, 20, string(types.ToRawJSON(err))))
		}
	}

	if err != nil {
		logger.Log.Warn().Err(err).Str("worker", sc.WorkerName).Msg("Closed socket due to failed to authorize the miner")
		sc.SocketClosedEvent <- struct{}{}
		return
	}

	if sc.JobManager.CurrentJob.JobId == "" {
		time.Sleep(10 * time.Second) // TODO: Move this to config
		return
	}

	sc.InitPoolDifficulty()
	sc.SendMiningJobEvent <- struct{}{}
}

// AuthorizeFn validates worker name and rig name.
func AuthorizeFn(ip net.Addr, port int, workerName string, password string) (authorized bool, options map[string]float64, err error) {
	logger.Log.Info().
		Str("workerName", workerName).
		Str("password", password).
		Str("ipAddr", ip.String()).
		Int("port", port).Msg("Authorize worker")

	var miner, rig string
	names := strings.Split(workerName, ".")
	if len(names) < 2 {
		miner = names[0]
		rig = "unknown"
	} else {
		miner = names[0]
		rig = names[1]
	}

	// JMP-64. Miner/rig validation.
	// Miner should be in UUID v4 format.
	_, err = uuid.Parse(miner)
	if err != nil {
		// TODO: use miner bitcoin address as worker name
		// return false, true, fmt.Errorf("%s", types.ErrMinerInvalidFormat)
	}

	// Rig is a string with [a-zA-Z0-9]+ regexp format.
	r, _ := regexp.Compile("[a-zA-Z0-9]+")
	if !r.MatchString(rig) {
		return false, nil, fmt.Errorf("%s", types.ErrRigInvalidFormat)
	}

	// Optional password. If it has format d={float64} then use this value as pool difficulty.
	if password != "" {
		options, err = utils.ExtractOptionsFromPassword(password)
		if err != nil {
			logger.Log.Info().Msg("invalid options in password, ignoring options")
			// return false, nil, fmt.Errorf("%s", types.ErrMinerPasswordInvalid)
			return true, map[string]float64{}, nil
		}

		return true, options, nil
	}

	return true, nil, nil
}

func (sc *Client) HandleSubmit(message *types.RPCRequest) {
	/* Avoid hash flood */
	if !sc.IsAuthorized {
		sc.SendJSON(types.ErrorResponse(message.ID, 24, "unauthorized worker"))
		sc.ShouldBan(false)
		return
	}

	// todo: do this on generic way.
	data, _ := json.Marshal(message)
	var request types.SubmitRequest
	err := json.Unmarshal(data, &request)
	if err != nil {
		logger.Log.Error().
			Err(err).Msg("unable to unmarshal request")
		sc.SendJSON(types.ErrorResponse(message.ID, 20, "unauthorized worker"))
	}

	// Increase connection deadline on each subsequent message
	deadline := time.Now().Add(time.Duration(sc.Options.Stratum.ConnectionTimeout) * time.Second)
	err = sc.Socket.SetDeadline(deadline)
	if err != nil {
		logger.Log.Error().Err(err).Str("from", sc.GetLabel()).Msg("Failed to increase connection deadline")
		return
	} else {
		logger.Log.Trace().Str("from", sc.GetLabel()).Int64("deadline", deadline.Unix()).Msg("Increased connection deadline")
	}

	submittedShare := WorkerSubmittedShare{
		Worker: sc.WorkerName,
	}

	now := time.Now()
	share, minerResult := sc.JobManager.
		ProcessSubmit(
			request.Params.JobID,
			sc.ExtraNonce1,                // extraNonce1 uint32
			request.Params.HexExtraNonce2, // hexExtraNonce2
			request.Params.HexNTime,       // hexNTime
			request.Params.HexNonce,       // hexNonce
			request.Params.VersionBits,    // versionBits
			sc.versionRolling,             // version rolling support
			sc.RemoteAddress,              // ipAddr net.Addr
			request.Params.WorkerName,     // workerName
			sc.ClientTarget,               // client target from connection options
		)
	logger.Log.Trace().Int64("duration", time.Since(now).Nanoseconds()).Msg("Profiling ProcessSubmit after optimisation")

	if share != nil {
		sc.varDiff.AppendShare(time.Now().Unix())

		// if share.ErrorCode != 0 {
		// 	logger.Log.Info().Str("worker name", sc.WorkerName).Str("JobId", share.JobId).Msg("submitted invalid share")
		// 	sc.SendWorkerSubmittedShare <- submittedShare
		// }

		if share.ErrorCode == types.ErrLowDiffShare {
			// warn the miner with current diff
			if sc.CurrentDifficulty != nil {
				diff, _ := sc.CurrentDifficulty.Float64()
				logger.Log.Warn().
					Float64("currDiff", diff).
					Msg("submitted share with low difficulty, discarding share")

			} else {
				logger.Log.Error().Msg("current difficulty is nil")
			}
			// TODO: Retarget difficulty
			// if sc.CurrentDifficulty != nil {
			// 	sc.SendDifficulty(sc.CurrentDifficulty)
			// } else {
			// 	logger.Log.Warn().Msg("current difficulty is nil")
			// }

			sc.SendWorkerSubmittedShare <- submittedShare
			return
		}

		if share.ErrorCode == types.ErrNTimeOutOfRange {
			// TODO: not sure what to do here
			// sc.SendMiningJob(sc.JobManager.CurrentJob.GetJobParams(true))
		}

		// Calculate next difficulty update value
		// if sc.varDiff != nil {
		// 	diff, _ := sc.CurrentDifficulty.Float64()
		// 	nextDiff := sc.varDiff.CalcNextDiff(diff)
		// 	logger.Log.Trace().Float64("diff", diff).Float64("nextDiff", nextDiff).Msg("Running variable difficulty check")
		// 	if nextDiff != diff && nextDiff != 0 {
		// 		logger.Log.Trace().Float64("difficulty", nextDiff).Msg("New pending difficulty")
		// 		sc.PendingDifficulty = big.NewFloat(nextDiff)
		// 		ok := sc.SendDifficulty(sc.PendingDifficulty)
		// 		if ok {
		// 			displayDiff, _ := sc.PendingDifficulty.Float64()
		// 			logger.Log.Trace().Str("worker", sc.WorkerName).
		// 				Float64("diff", displayDiff).Msg("Sent new difficulty")
		// 			sc.PendingDifficulty = nil
		// 		} else {
		// 			logger.Log.Error().Str("worker", sc.WorkerName).Msg("Failed to send new difficulty")
		// 		}
		// 	}
		// }

		if sc.ShouldBan(share.ErrorCode == 0) {
			return
		}

		var errParams *types.JsonRpcError
		if share.ErrorCode != 0 {
			errParams = &types.JsonRpcError{
				Code:    int(share.ErrorCode),
				Message: share.ErrorCode.String(),
			}

			logger.Log.Error().Str("error message", errParams.Message).Str("worker name", sc.WorkerName).
				Msg("share is invalid")
			sc.SendJSON(types.ErrorResponse(message.ID, int(share.ErrorCode), share.ErrorCode.String()))
			sc.SendWorkerSubmittedShare <- submittedShare
			return
		}

		// logger.Log.Info().Str("worker name", sc.WorkerName).Str("JobId", share.JobId).Msg("submitted a valid share")
		sc.SendJSON(types.AuthResponse(message.ID, true))

		// update worker stats
		submittedShare.ValidShare = true
		sc.SendWorkerSubmittedShare <- submittedShare

		// send result to channel if at least one block found
		if minerResult.ContainsMinedBitcoinBlock() || minerResult.ContainsMinedBeaconBlock() || minerResult.ContainsMinedShardsBlocks() {
			logger.Log.Info().Str("worker name", sc.WorkerName).Str("JobId", share.JobId).Msg("sent a valid share to miner result channel")
			sc.JobManager.MinerResultChan <- minerResult
		} else {
			logger.Log.Info().Str("worker name", sc.WorkerName).Str("JobId", share.JobId).Msg("submitted a valid share but no block mined")
		}
	}
}

func (sc *Client) SendJSON(data interface{}) {
	raw, err := json.Marshal(data)
	if err != nil {
		logger.Log.Error().Err(err).Interface("data", data).Msg("failed to serialize JSON")
		return
	}

	message := make([]byte, 0, len(raw)+1)
	message = append(raw, '\n')
	sc.SocketBufIOMutex.Lock()
	_, err = sc.SocketBufIO.Write(message)
	if err != nil {
		sc.SocketBufIOMutex.Unlock()
		logger.Log.Error().Err(err).Str("raw_data", string(raw)).Msg("failed inputting")
		sc.SocketClosedEvent <- struct{}{}
		return
	}

	err = sc.SocketBufIO.Flush()
	if err != nil {
		sc.SocketBufIOMutex.Unlock()
		logger.Log.Error().Err(err).Str("worker_name", sc.WorkerName).Msg("failed sending data for worker")
		sc.SocketClosedEvent <- struct{}{}
		return
	}

	sc.SocketBufIOMutex.Unlock()
	logger.Log.Debug().Str("raw data", string(raw)).Msg("sent raw bytes")
}

func (sc *Client) ShouldBan(shareValid bool) bool {
	if shareValid {
		sc.Shares.Valid++
	} else {
		sc.Shares.Invalid++
		if sc.Shares.TotalShares() >= sc.Options.Stratum.Banning.CheckThreshold {
			if sc.Shares.BadPercent() < sc.Options.Stratum.Banning.InvalidPercent {
				sc.Shares.Reset()
			} else {
				logger.Log.Info().Str("invalid shares", strconv.FormatUint(sc.Shares.Invalid, 10)).Str("total shares", strconv.FormatUint(sc.Shares.TotalShares(), 10)).Send()
				sc.BanningManager.AddBannedIP(sc.RemoteAddress.String())
				logger.Log.Warn().Str("", sc.WorkerName).Msg("closed socket due to shares bad percent reached the banning invalid percent threshold")
				sc.SocketClosedEvent <- struct{}{}
				return true
			}
		}
	}

	return false
}

func (sc *Client) GetLabel() string {
	if sc.WorkerName != "" {
		return sc.WorkerName + " [" + sc.RemoteAddress.String() + "]"
	} else {
		return "(unauthorized)" + " [" + sc.RemoteAddress.String() + "]"
	}
}

func BitsToStr(bits uint32) string {
	target := pow.CompactToBig(bits)
	return fmt.Sprintf("%064x", target)
}

func (sc *Client) SendDifficulty(diff *big.Float) bool {
	if diff == nil {
		logger.Log.Error().Msg("trying to send empty diff!")
		return false
	}

	sc.CurrentDifficulty = diff
	f, _ := diff.Float64()
	sc.SendJSON(types.SetDifficultyResponse(0, f)) // TODO: Send correct id

	return true
}

// InitPoolDifficulty calculates initial pool difficulty when new
// stratum client is connected and sends it to the client.
func (sc *Client) InitPoolDifficulty() {

	minDiff := big.NewFloat(minPoolDifficulty)
	var poolDiff *big.Float

	if sc.Options.Stratum.Ports.VarDiff.Enabled {
		// Variable difficulty
		if sc.varDiff.JobPriority {
			poolDiff = minDiff
			sc.varDiff.CurrDiff = minPoolDifficulty
		} else {
			if sc.varDiff.FixDiff > 0.0 {
				poolDiff = big.NewFloat(sc.varDiff.FixDiff)
			} else {
				poolDiff = big.NewFloat(sc.varDiff.StartDiff)
				sc.varDiff.CurrDiff = sc.varDiff.StartDiff
			}
		}
	} else {
		// Fixed difficulty from password options or fixed minimum difficulty
		if sc.CurrentDifficulty != nil {
			poolDiff = sc.CurrentDifficulty
		} else {
			poolDiff = minDiff
		}
		// logger.Log.Trace().Msg("Vardiff disabled, sending fixed diff")
	}

	sc.ClientTarget = &sc.JobManager.CurrentJob.MinerTask.ShareTarget
	sc.SendDifficulty(poolDiff)
	logger.Log.Trace().
		Str("poolDiff", poolDiff.String()).
		Str("sc.ClientTarget", sc.ClientTarget.String()).
		Msg("InitPoolDifficulty")
}

func (sc *Client) SendMiningJob(jobParams types.JobParams) {
	if !sc.IsAuthorized {
		logger.Log.Trace().Str("from", sc.GetLabel()).Msg("Client is not authorised, skip sending job")
		return
	}

	lastActivityAgo := time.Since(sc.LastActivity)
	if lastActivityAgo > time.Duration(sc.Options.Stratum.ConnectionTimeout)*time.Second {
		sc.SocketClosedEvent <- struct{}{}
		logger.Log.Info().Str("sc.WorkerName", sc.WorkerName).Msg("Closed socket due to activity timeout")
		metrics.IncCounterVec(metrics.WorkerConnTimeout, map[string]string{"worker": sc.WorkerName})
		return
	}

	// TODO: Retarget difficulty
	// if sc.PendingTarget != 0 {
	// 	ok := sc.SendDifficulty(sc.PendingTarget)
	// 	sc.PendingTarget = 0
	// 	if ok {
	// 		// difficultyChanged
	// 		// -> difficultyUpdate client.workerName, diff
	// 		logger.Log.Info().Str("worker", sc.WorkerName).
	// 			Uint32("diff", sc.PendingTarget).Msg("Update Difficulty")
	// 	}
	// }

	sc.SendJSON(types.NotifyMessageReq(0, jobParams))
}

func (sc *Client) ManuallyAuthClient(username, password string) {
	sc.HandleAuthorize(&types.RPCRequest{
		ID:     1,
		Method: "",
		Params: []json.RawMessage{types.ToRawJSON(username), types.ToRawJSON(password)},
	}, false)
}

func (sc *Client) ManuallySetValues(otherClient *Client) {
	sc.ExtraNonce1 = otherClient.ExtraNonce1
	sc.PreviousTarget = otherClient.PreviousTarget
	sc.CurrentTarget = otherClient.CurrentTarget
}

func (sc *Client) HandleUpdateBlock(message *types.RPCRequest) {
	if len(message.Params) < 2 {
		logger.Log.Trace().Str("reason", "not enough params").Msg("Update block method failed")
		sc.SendJSON(types.ErrorResponse(message.ID, 20, string(types.ToRawJSON("not enough params"))))
		return
	}

	token := string(message.Params[0])
	token = strings.TrimPrefix(token, `"`)
	token = strings.TrimSuffix(token, `"`)
	if token != sc.Options.Stratum.UpdateBlockToken {
		logger.Log.Trace().Str("want", sc.Options.Stratum.UpdateBlockToken).Str("got", token).Msg("Invalid update block token received")
		sc.SendJSON(types.ErrorResponse(message.ID, 24, string(types.ToRawJSON("invalid update block token"))))
		return
	}

	chain := string(message.Params[1])
	chainID, err := strconv.ParseInt(chain, 10, 64)
	if err != nil {
		logger.Log.Trace().Msg("Invalid chainID received")
		sc.SendJSON(types.ErrorResponse(message.ID, 24, string(types.ToRawJSON("invalid chain ID"))))
		return
	}

	sc.sendStratumUpdateBlockEvent <- events.StratumUpdateBlock{ChainID: chainID}
	sc.SendJSON(types.AuthResponse(message.ID, true))
}

// HandleConfigure handles mining.configure stratum method.
// Currently only version-rolling extension is supported.
// min-bit-count field is considered obsolete and does not use in the code.
// Example of this request:
// {
//		"method": "mining.configure", "id": 1,
//		"params": [
//			["minimum-difficulty", "version-rolling"],
//			{
//				"minimum-difficulty.value":      2048,
//				"version-rolling.mask":          "1fffe000",
//				"version-rolling.min-bit-count": 2
//			}
//		]
// }
func (sc *Client) HandleConfigure(message *types.RPCRequest) {
	if len(message.Params) == 2 {
		extensions := string(message.Params[0])
		if strings.Contains(extensions, "version-rolling") {
			req := &types.VersionRollingReq{}
			err := json.Unmarshal(message.Params[1], req)
			if err != nil {
				logger.Log.Error().Err(err).Msg("Unable to unmarshal version rolling request")
				sc.SendJSON(types.ErrorResponse(message.ID, 24, string(types.ToRawJSON("unable to decode rolling request"))))
				return
			}

			mask, err := strconv.ParseUint(req.Mask, 16, 32)
			if err != nil {
				sc.SendJSON(types.ErrorResponse(message.ID, 24, string(types.ToRawJSON("invalid version rolling mask value"))))
				return
			}

			var clientMask uint32
			clientMask = versionRollingServerMask & uint32(mask)

			sc.versionRolling = types.VersionRolling{
				Supported: true,
				Mask:      clientMask,
			}

			sc.SendJSON(types.VersionRollingResponse(message.ID, clientMask))
		}
	} else {
		sc.SendJSON(types.ErrorResponse(message.ID, 24, string(types.ToRawJSON("invalid configure request"))))
		return
	}
}

// VarDiffStart starts variable difficulty feature.
// Every RetargetTimeWindow represented in seconds it calculates next client difficulty.
// It stops when receive a data via stopChan channel.
func (sc *Client) VarDiffStart() {
	if !sc.varDiff.Enabled {
		logger.Log.Trace().Msg("VarDiff disabled, return")
		return
	}

	go func() {
		dur := time.Second * time.Duration(sc.varDiff.RetargetTimeWindow)
		retargetTimeWindowTicker := time.NewTicker(dur)
		defer retargetTimeWindowTicker.Stop()

		for {
			select {
			case <-sc.varDiff.StopChan:
				return

			case <-retargetTimeWindowTicker.C:
				logger.Log.Trace().Msg("VarDiff CalcNextDiff")
				sc.varDiff.CalcNextDiff()
				if sc.varDiff.NewDiff != sc.varDiff.CurrDiff {
					logger.Log.Trace().Float64("sc.varDiff.CurrDiff", sc.varDiff.CurrDiff).Float64("sc.varDiff.NewDiff", sc.varDiff.NewDiff).Msg("VarDiff updated")
					bigDiff := big.NewFloat(sc.varDiff.NewDiff)
					sc.ClientTarget = utils.Difficulty2Target(bigDiff)
					sc.varDiff.CurrDiff = sc.varDiff.NewDiff
					sc.SendDifficulty(bigDiff)
				}
			}
		}
	}()
}

func (sc *Client) VarDiffStop() {
	sc.varDiff.StopChan <- struct{}{}
	close(sc.varDiff.StopChan)
}
