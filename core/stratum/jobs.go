package stratum

import (
	"math/big"
	"net"
	"time"

	"gitlab.com/jaxnet/core/miner/core/logger"
	"gitlab.com/jaxnet/core/miner/core/miner/tasks"
	"gitlab.com/jaxnet/core/miner/core/stratum/types"
	"gitlab.com/jaxnet/core/miner/core/utils"
)

type JobCounter struct {
	Counter *big.Int
}

type Job struct {
	MinerTask tasks.MinerTask
	// types.JobParams
	JobId                 string
	PrevHashReversed      string
	Submits               map[string]struct{}
	GenerationTransaction [][]byte
	Broadcasted           bool
	Bits                  uint32 // Bitcoin block bits
}

func NewJob(task *tasks.MinerTask) (job *Job) {
	bitcoinBlockTarget := task.BitcoinBlockTarget()
	logger.Log.Trace().Str("value", bitcoinBlockTarget.Text(16)).Msg("BitcoinBlockTarget")

	MaxTargetTruncated, _ := new(big.Int).SetString("00000000FFFF0000000000000000000000000000000000000000000000000000", 16)
	bigDiff := new(big.Float).Quo(
		new(big.Float).SetInt(MaxTargetTruncated),
		new(big.Float).SetInt(&bitcoinBlockTarget),
	)

	logger.Log.Trace().Str("value", bigDiff.String()).Msg("BitcoinBlockDifficulty")
	logger.Log.Trace().Str("value", task.BeaconTarget.Text(16)).Msg("BeaconTarget")

	// Pre-calculate bitcoin coinbase transaction parts
	signatureScript := task.BitcoinBlock.Transactions[0].TxIn[0].SignatureScript

	heightLenIdx := 0
	heightLen := int(signatureScript[heightLenIdx])
	if heightLen > 0xF {
		// if value more than 0xF,
		// this indicates that height is packed as an opcode OP_0 .. OP_16 (small int)
		// height = int(rawTx[heightLenIdx] - (txscript.OP_1 - 1))
		// so height value doesn't gives additional padding
		heightLen = 0
	}
	extraNonceLenIdx := heightLenIdx + 1 + heightLen
	extraNonceLen := int(signatureScript[extraNonceLenIdx])

	task.BitcoinBlockCoinbaseParts = utils.BitcoinBlockCoinbaseParts{
		Head: signatureScript[0:extraNonceLenIdx],
		Tail: signatureScript[extraNonceLenIdx+extraNonceLen+1:],
	}

	maxTarget := bitcoinBlockTarget
	if len(task.ShardsTargets) > 0 {
		if task.BeaconTarget.Cmp(&bitcoinBlockTarget) > 0 {
			if task.BeaconTarget.Cmp(&task.ShardsTargets[0].Target) > 0 {
				maxTarget = task.BeaconTarget
			} else {
				maxTarget = task.ShardsTargets[0].Target
			}
		} else {
			if bitcoinBlockTarget.Cmp(&task.ShardsTargets[0].Target) <= 0 {
				maxTarget = task.ShardsTargets[0].Target
			}
		}
	} else {
		if task.BeaconTarget.Cmp(&bitcoinBlockTarget) > 0 {
			maxTarget = task.BeaconTarget
		}
	}

	task.ShareTarget = maxTarget
	task.BitcoinBlock.Header.Timestamp = time.Now()

	job = &Job{
		JobId:     utils.RandHexUint64(),
		MinerTask: *task,
		Bits:      task.BitcoinBlock.Header.Bits,
		Submits:   make(map[string]struct{}),
	}
	return job
}

type Share struct {
	JobId      string          `json:"jobId"`
	RemoteAddr net.Addr        `json:"remoteAddr"`
	Miner      string          `json:"miner"`
	Rig        string          `json:"rig"`
	ErrorCode  types.ErrorWrap `json:"errorCode"`
	DiffBigInt *big.Int        `json:"diffBigInt"`
	// TODO: not sure if fields below are needed
	BlockHeight int64   `json:"height"`
	BlockReward uint64  `json:"blockReward"`
	Diff        float64 `json:"shareDiff"`
	BlockHash   string  `json:"blockHash"`
	BlockHex    string  `json:"blockHex"`
	TxHash      string  `json:"txHash"`
}

func (j *Job) RegisterSubmit(extraNonce1, extraNonce2, nTime, nonce string) bool {
	submission := extraNonce1 + extraNonce2 + nTime + nonce

	if _, ok := j.Submits[submission]; !ok {
		j.Submits[submission] = struct{}{}
		return true
	}

	return false
}
