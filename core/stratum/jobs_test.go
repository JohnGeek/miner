package stratum

import (
	"bytes"
	"encoding/hex"
	"fmt"
	"math"
	"math/big"
	"testing"

	btcdwire "github.com/btcsuite/btcd/wire"
	"gitlab.com/jaxnet/core/miner/core/logger"
	"gitlab.com/jaxnet/core/miner/core/miner/tasks"
	"gitlab.com/jaxnet/core/miner/core/settings"
	"gitlab.com/jaxnet/core/miner/core/utils"
	"gitlab.com/jaxnet/jaxnetd/types/chainhash"
)

func TestCheckBTCSolution(t *testing.T) {
	blockData := "00000020f67ad7695d9b662a72ff3d8edbbb2de0bfa67b13974bb9910d116d5cbd863e682465956c3386cd9dec8520e61aeabb52913eb996cffc6c5b09517aedcdecec270104e860ffff0f1e000000000101000000010000000000000000000000000000000000000000000000000000000000000000ffffffff125100000e2f503253482f6a61786e6574642fffffffff030000000000000000176a152068747470733a2f2f6a61782e6e6574776f726b2000f2052a01000000176a1520202020202020204a41582020202020202020202000000000000000001976a914b953dad0e79288eea918085c9b72c3ca5482349388ac00000000" +
		"dad0e79288eea918085c9b72c3ca5482349388ac00000000"
	raw, _ := hex.DecodeString(blockData)
	buf := bytes.NewBuffer(raw)
	block := btcdwire.MsgBlock{}
	err := block.Deserialize(buf)
	if err != nil {
		t.Log(err)
		t.FailNow()
		return
	}

	block, _ = utils.UpdateBitcoinExtraNonce(block, 0x42, 0xBAD, chainhash.ZeroHash.CloneBytes())

	buf = bytes.NewBuffer(nil)
	block.Transactions[0].Serialize(buf)
	rawTx := buf.Bytes()

	// heightLenIdx := 42
	// heightLen := int(rawTx[heightLenIdx])
	// extraNonceLenIdx := heightLenIdx + 1 + heightLen
	// extraNonceLen := int(rawTx[extraNonceLenIdx])

	// part1 := rawTx[0:extraNonceLenIdx]
	// part2 := rawTx[extraNonceLenIdx+extraNonceLen+1:]

	// fmt.Printf("%0x\n", heightLen)
	// fmt.Println(hex.EncodeToString(rawTx[heightLenIdx+1 : heightLenIdx+heightLen+1]))
	// fmt.Printf("%0x\n", extraNonceLen)
	// fmt.Println(hex.EncodeToString(rawTx[extraNonceLenIdx+1 : extraNonceLenIdx+extraNonceLen+1]))
	//

	part1, part2 := utils.SplitCoinbase(block)
	fmt.Println(hex.EncodeToString(rawTx))
	fmt.Println(hex.EncodeToString(part1))
	fmt.Println(hex.EncodeToString(part2))

	signatureScript := block.Transactions[0].TxIn[0].SignatureScript

	extraNonceLen := int(signatureScript[0])
	fmt.Println(extraNonceLen)
	fmt.Println(hex.EncodeToString(signatureScript[1 : extraNonceLen+1]))
	fmt.Println(hex.EncodeToString(signatureScript[extraNonceLen+1:]))
	fmt.Println(hex.EncodeToString(signatureScript))

	// fmt.Println(hex.EncodeToString(block.Transactions[0].TxIn[0].SignatureScript))
	// fmt.Println(hex.EncodeToString(rawTx))

	task := tasks.MinerTask{
		BitcoinBlock: block,
		// BitcoinBlockBits:   block.Header.Bits,
		// BitcoinBlockTarget: pow.CompactToBig(block.Header.Bits),
		BitcoinBlockHeight: 42,
		BlockFlags:         tasks.BitcoinFlag,
	}

	return
	logger.Init(logger.LogParams{})
	config := &settings.Configuration{}
	minerResultChan := make(chan tasks.MinerResult)
	jm := NewJobManager(config, minerResultChan)
	nonce := uint32(0)
	extraNonce := uint64(0)
	for ; extraNonce < math.MaxUint64; extraNonce++ {
		for ; nonce < math.MaxUint32; nonce++ {

			result, atLeastOneBlockMined := jm.CheckSolution(task, nonce, block.Header.Timestamp, 0, 0, false, 0, big.NewInt(0))
			if atLeastOneBlockMined || result.ContainsMinedBitcoinBlock() {
				t.Log("bits:", task.BitcoinBlock.Header.Bits)
				target := task.BitcoinBlockTarget()
				t.Log("target:", "%s", target.Text(16))
				t.Log("nonce:", nonce)
				t.Log("extra_nonce:", extraNonce)
				t.Log("hash:", result.BitcoinBlock.BlockHash().String())
				return
			}
		}
	}
	// t.Log("nonce:", nonce)
	// t.Log("extra_nonce:", extraNonce)
}
