package types

type ErrorWrap int

const (
	ErrJobNotFound               ErrorWrap = 20
	ErrIncorrectExtraNonce2Size  ErrorWrap = 21
	ErrIncorrectNTimeSize        ErrorWrap = 22
	ErrNTimeOutOfRange           ErrorWrap = 23
	ErrIncorrectNonceSize        ErrorWrap = 24
	ErrDuplicateShare            ErrorWrap = 25
	ErrLowDiffShare              ErrorWrap = 26
	ErrMinerInvalidFormat        ErrorWrap = 27
	ErrRigInvalidFormat          ErrorWrap = 28
	ErrBitcoinBlockInvalidHash   ErrorWrap = 29
	ErrMinerPasswordInvalid      ErrorWrap = 30
	ErrIncorrectVersionBits      ErrorWrap = 31
	ErrIncorrectVersionBitsValue ErrorWrap = 32
)

var codeToErrMap = map[int]string{
	10: "you are banned by pool",
	20: "job not found",
	21: "incorrect size of extranonce2",
	22: "incorrect size of ntime",
	23: "ntime out of range",
	24: "incorrect size of nonce",
	25: "duplicate share",
	26: "low difficulty share",
	27: "miner should be in UUID v4 format",
	28: "rig should be in [a-zA-Z0-9]+ format",
	29: "bitcoin block mined with invalid hash",
	30: "password should be in d={float64} format",
	31: "incorrect size of versionBits",
	32: "incorrect versionBits value",
}

func (err ErrorWrap) String() string {
	return codeToErrMap[int(err)]
}

type VersionRolling struct {
	Supported bool
	Mask      uint32
}
