package types

import (
	"encoding/json"

	"gitlab.com/jaxnet/core/miner/core/logger"
)

type BaseResponse struct {
	Id    interface{}   `json:"id"` // can be int64, string or null
	Error *JsonRpcError `json:"error,omitempty"`
}

type RPCResponse struct {
	ID     interface{}     `json:"id"` // can be int64, string or null
	Result json.RawMessage `json:"result,omitempty"`
	Error  *JsonRpcError   `json:"error,omitempty"`
}

type RPCRequest struct {
	ID     interface{}       `json:"id"` // can be int64, string or null
	Method string            `json:"method"`
	Params []json.RawMessage `json:"params"`
}

type LoginMessage struct {
	Login string `json:"login"`
	Pass  string `json:"pass"`
	Agent string `json:"agent"`
}

// RPCLoginMessage describes non-standard authentication method
// {"id":1,"jsonrpc":"2.0","method":"login","params":{"login":"t.r10_500000","pass":"d=1800000","agent":"GK_Invoker"}}
type RPCLoginMessage struct {
	ID     interface{}  `json:"id"` // can be int64, string or null
	Method string       `json:"method"`
	Params LoginMessage `json:"params"`
}

type JsonRpcError struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

type VersionRollingReq struct {
	Mask        string `json:"version-rolling.mask"`
	MinBitCount uint32 `json:"version-rolling.min-bit-count"`
}

// TODO: Copied from utils. Probably move to separate file in this package.

// ToRawJSON encodes i to []byte // todo: remove this
func ToRawJSON(i interface{}) []byte {
	r, err := json.Marshal(i)
	if err != nil {
		logger.Log.Error().Err(err).Msg("ToRawJSON")
		return nil
	}

	return r
}
