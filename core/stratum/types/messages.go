package types

import (
	"encoding/hex"
	"encoding/json"
	"fmt"
	"strconv"

	"gitlab.com/jaxnet/core/miner/core/utils"
	"gitlab.com/jaxnet/jaxnetd/types/pow"
)

// Client                                Server
//  |                                     |
//  | --------- mining.subscribe -------> |
//  | --------- mining.authorize -------> |
//  | <-------- mining.set_target ------- |
//  |                                     |----
//  | <---------- mining.notify --------- |<--/
//  |                                     |
//  | ---------- mining.submit ---------> |

const (
	Subscribe           = "mining.subscribe"
	ExtranonceSubscribe = "mining.extranonce.subscribe"
	Authorize           = "mining.authorize"
	SetDifficulty       = "mining.set_difficulty"
	SetTarget           = "mining.set_target"
	SetExtranonce       = "mining.set_extranonce"
	Configure           = "mining.configure"
	Submit              = "mining.submit"
	Notify              = "mining.notify"
	SetVersionMask      = "mining.set_version_mask"
	UpdateBlock         = "mining.update_block"
)

type SubscribeResp struct {
	ID     interface{}     `json:"id"`
	Result json.RawMessage `json:"result"`
}

type AuthReq struct {
	ID     interface{} `json:"id"`
	Method string      `json:"method"`
	Params []string    `json:"params"`
}

func (req AuthReq) Name() string {
	if len(req.Params) < 1 {
		return ""
	}
	return req.Params[0]
}

func (req AuthReq) Password() string {
	if len(req.Params) < 2 {
		return ""
	}
	return req.Params[1]
}

type AuthResp struct {
	ID     interface{} `json:"id"`
	Result bool        `json:"result"`
}

func AuthResponse(id interface{}, isAuthorized bool) AuthResp {
	return AuthResp{ID: id, Result: isAuthorized}
}

func SetDifficultyResponse(id interface{}, difficulty float64) RPCRequest {
	diff, _ := json.Marshal(difficulty)
	return RPCRequest{
		ID:     id,
		Method: SetDifficulty,
		Params: []json.RawMessage{diff},
	}
}

func SubscribeResponse(id interface{}, subscriptionId uint64, extraNonce1 uint32) SubscribeResp {
	subscription := strconv.FormatUint(subscriptionId, 10)
	n := utils.PackUint32LE(extraNonce1)
	nonce := hex.EncodeToString(n)

	return SubscribeResp{
		ID: id,
		Result: ToRawJSON([]interface{}{
			[][]string{
				{SetDifficulty, subscription},
				{Notify, subscription},
			},
			// "08000002",
			nonce, // 4
			4,     // 4
		}),
	}
}

func ErrorResponse(id interface{}, code int, message string) RPCResponse {
	return RPCResponse{
		ID:     id,
		Result: nil,
		Error: &JsonRpcError{
			Code:    code,
			Message: message,
		},
	}
}

func BitsToStr(bits uint32) string {
	target := pow.CompactToBig(bits)
	return fmt.Sprintf("%064x", target)
}

// JobParams is result structure for method: "mining.notify":
// -- "bf",
// -- "4d16b6f85af6e2198f44ae2a6de67f78487ae5611b77c6c0440b921e00000000",
// -- "01000000010000000000000000000000000000000000000000000000000000000000000000ffffffff20020862062f503253482f04b8864e5008",
// -- "072f736c7573682f000000000100f2052a010000001976a914d23fcdf86f7e756a64a7a9688ef9903327048ed988ac00000000",
// -- [],
// -- "00000002",
// -- "1c2ac4af",
// -- "504e86b9",
// -- false
//
// -- JobID - ID of the job. Use this ID while submitting share generated from this job.
// -- PrevHash - Hash of previous block.
// -- CoinbaseP1 - Initial part of coinbase transaction.
// -- CoinbaseP2 - Final part of coinbase transaction.
// -- MerkleBranch - List of hashes, will be used for calculation of merkle root.
// This is not a list of all transactions, it only contains prepared hashes of steps of merkle tree algorithm.
// -- Version - Bitcoin block version.
// -- Bits - Encoded current network difficulty
// -- Time - Current ntime/
// -- CleanJobs - When true, server indicates that submitting shares from previous jobs don't have a sense and
// such shares will be rejected. When this flag is set, miner should also drop all previous jobs,
// so job_ids can be eventually rotated.
//
// Coinb1 + Extranonce1 + Extranonce2 + Coinb2
type JobParams struct {
	JobID        string
	PrevHash     string
	CoinbaseP1   []byte
	CoinbaseP2   []byte
	MerkleBranch []string
	Version      int32
	Bits         uint32
	Time         uint32
	CleanJobs    bool
}

func (j JobParams) MarshalJSON() ([]byte, error) {
	data := []interface{}{
		j.JobID,
		j.PrevHash,
		hex.EncodeToString(j.CoinbaseP1),
		hex.EncodeToString(j.CoinbaseP2),
		j.MerkleBranch,
		hex.EncodeToString(utils.PackInt32BE(j.Version)),
		hex.EncodeToString(utils.PackUint32BE(j.Bits)),
		hex.EncodeToString(utils.PackUint32BE(j.Time)),
		j.CleanJobs,
	}
	return json.Marshal(data)
}

type NotifyMessage struct {
	ID     interface{} `json:"id"`
	Method string      `json:"method"`
	Params JobParams   `json:"params"`
}

func NotifyMessageReq(id interface{}, j JobParams) NotifyMessage {
	return NotifyMessage{
		ID:     id,
		Method: Notify,
		Params: j,
	}
}

type SubmitRequest struct {
	ID     interface{}  `json:"id"`
	Method string       `json:"method"`
	Params SubmitParams `json:"params"`
}

type SubmitParams struct {
	WorkerName     string
	JobID          string
	HexExtraNonce2 string
	HexNTime       string
	HexNonce       string
	VersionBits    string
}

func (s *SubmitParams) UnmarshalJSON(bytes []byte) error {
	params := [6]string{}
	err := json.Unmarshal(bytes, &params)
	if err != nil {
		return err
	}
	s.WorkerName = params[0]
	s.JobID = params[1]
	s.HexExtraNonce2 = params[2]
	s.HexNTime = params[3]
	s.HexNonce = params[4]
	s.VersionBits = params[5]
	return nil
}

func (s SubmitParams) MarshalJSON() ([]byte, error) {
	return json.Marshal([]string{
		s.WorkerName,
		s.JobID,
		s.HexExtraNonce2,
		s.HexNTime,
		s.HexNonce,
	})
}

type VersionRollingRespResult struct {
	VersionRolling     bool   `json:"version-rolling"`
	VersionRollingMask string `json:"version-rolling.mask"`
}

type VersionRollingResp struct {
	ID     interface{}              `json:"id"`
	Error  interface{}              `json:"error"`
	Result VersionRollingRespResult `json:"result"`
}

func VersionRollingResponse(id interface{}, versionRollingMask uint32) VersionRollingResp {
	mask := fmt.Sprintf("%08x", versionRollingMask)
	return VersionRollingResp{
		ID:    id,
		Error: nil,
		Result: VersionRollingRespResult{
			VersionRolling:     true,
			VersionRollingMask: mask,
		},
	}
}
