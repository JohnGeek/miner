package stratum

import (
	"bytes"
	"encoding/binary"
	"encoding/hex"
	"math/big"
	"net"
	"sort"
	"strconv"
	"strings"
	"time"

	btcdchainhash "github.com/btcsuite/btcd/chaincfg/chainhash"
	btcdutil "github.com/btcsuite/btcutil"
	"github.com/patrickmn/go-cache"
	"gitlab.com/jaxnet/core/miner/core/common"
	"gitlab.com/jaxnet/core/miner/core/logger"
	"gitlab.com/jaxnet/core/miner/core/miner/tasks"
	"gitlab.com/jaxnet/core/miner/core/settings"
	"gitlab.com/jaxnet/core/miner/core/stratum/types"
	"gitlab.com/jaxnet/core/miner/core/utils"
	"gitlab.com/jaxnet/jaxnetd/types/chainhash"
	"gitlab.com/jaxnet/jaxnetd/types/pow"
)

type JobManager struct {
	Config                *settings.Configuration
	JobCounter            *JobCounter
	ExtraNoncePlaceholder []byte
	ExtraNonceSize        int // 4
	CurrentJob            Job
	ValidJobsCache        *cache.Cache
	CoinbaseHasher        func([]byte) chainhash.Hash // Not sure if this needed
	MinerResultChan       chan tasks.MinerResult
	hashSortingIndexes    []HashSortingIndexesRange
}

type HashSortingIndexesRange struct {
	SmallestIndex uint32
	BiggestIndex  uint32
}

const hashSortingBeaconIndex = 0

func NewJobManager(config *settings.Configuration, minerResultChan chan tasks.MinerResult) *JobManager {
	placeholder, _ := hex.DecodeString("f000000ff111111f")

	jm := JobManager{
		Config:                config,
		ExtraNoncePlaceholder: placeholder,
		ExtraNonceSize:        4,
		CurrentJob:            Job{},
		ValidJobsCache:        cache.New(avgBitcoinBlockMiningIntervalExpiration*time.Minute, avgBitcoinBlockMiningIntervalPurge*time.Minute),
		CoinbaseHasher:        chainhash.DoubleHashH,
		MinerResultChan:       minerResultChan,
	}
	jm.initOptimizedHashSortingShardsIndexesStorage()

	return &jm
}

func (jm *JobManager) GetJobParamsEx(job Job, forceUpdate bool) (types.JobParams, error) {
	btcBlock := job.MinerTask.BitcoinBlock
	logger.Log.Info().Str("hash:", btcBlock.Header.PrevBlock.String()).Msg("Prev Block Header")

	if jm.Config.Log.Debug {
		buf := bytes.NewBuffer(nil)
		btcBlock.Header.Serialize(buf)

		buf = bytes.NewBuffer(nil)
		btcBlock.Transactions[0].Serialize(buf)
		logger.Log.Info().Str("value", hex.EncodeToString(buf.Bytes())).Msg("Send Job Coinbase")
	}

	part1, part2 := utils.SplitCoinbase(btcBlock)

	block := btcdutil.NewBlock(&btcBlock)
	transactions := block.Transactions()

	merkleBranch := make([]string, 0)
	logger.Log.Trace().Int("transactions", len(transactions)).Msg("New block candidate")

	if len(transactions) > 1 {
		// merkles := blockchain.BuildMerkleTreeStore(transactions[1:], false)
		// merklePath := utils.MerkleTreeBranch(merkles)
		// merkleBranch := make([]string, len(merklePath))
		txsBytes := make([][]byte, 0)
		for _, transaction := range transactions {
			txsBytes = append(txsBytes, transaction.Hash().CloneBytes())
		}

		merkleSteps := utils.CalculateSteps(txsBytes)
		merkleBranch = utils.GetMerkleHashes(merkleSteps)

		logger.Log.Trace().
			Int("merkleSteps", len(merkleSteps)).
			Int("txLen:", len(txsBytes)).
			Int("merkleBranchLen", len(merkleBranch)).
			Msg("Job merkleBranch")
	}

	jm.ValidJobsCache.Set(job.JobId, &job, cache.DefaultExpiration)

	return types.JobParams{
		JobID: job.JobId,
		PrevHash: hex.EncodeToString(
			utils.ReverseByteOrder(
				utils.ReverseBytes(btcBlock.Header.PrevBlock[:]),
			),
		),
		CoinbaseP1:   part1,
		CoinbaseP2:   part2,
		MerkleBranch: merkleBranch,
		Version:      btcBlock.Header.Version,
		Bits:         job.Bits,
		Time:         uint32(btcBlock.Header.Timestamp.Unix()),
		CleanJobs:    forceUpdate,
	}, nil
}

func (jm *JobManager) ProcessSubmit(jobId string,
	extraNonce1 uint32, hexExtraNonce2, hexNTime, hexNonce, hexVersionBits string,
	versionRolling types.VersionRolling, ipAddr net.Addr, workerName string, clientTarget *big.Int) (
	share *Share, minerResult tasks.MinerResult) {
	minerResult = tasks.NewMinerResult()

	submitTime := time.Now()

	logger.Log.Info().
		Str("jobId", jobId).
		Uint32("extraNonce1", extraNonce1).
		Str("hexExtraNonce2", hexExtraNonce2).
		Str("hexNTime", hexNTime).
		Str("hexNonce", hexNonce).
		Str("versionBits", hexVersionBits).
		Interface("ipAddr", ipAddr).
		Str("workerName", workerName).Msg("Process submitted block")

	var miner, rig string
	names := strings.Split(workerName, ".")
	if len(names) < 2 {
		miner = names[0]
		rig = "unknown"
	} else {
		miner = names[0]
		rig = names[1]
	}

	cachedJob, exists := jm.ValidJobsCache.Get(jobId)
	if !exists {
		return &Share{
			JobId:      jobId,
			RemoteAddr: ipAddr,
			Miner:      miner,
			Rig:        rig,

			ErrorCode: types.ErrJobNotFound,
		}, minerResult
	}

	job := *cachedJob.(*Job)
	if job.JobId != jobId {
		return &Share{
			JobId:      jobId,
			RemoteAddr: ipAddr,
			Miner:      miner,
			Rig:        rig,

			ErrorCode: types.ErrJobNotFound,
		}, minerResult
	}

	if hexNTime == "" {
		return &Share{
			JobId:      jobId,
			RemoteAddr: ipAddr,
			Miner:      miner,
			Rig:        rig,

			ErrorCode: types.ErrIncorrectNTimeSize,
		}, minerResult
	}

	rawTime, err := hex.DecodeString(hexNTime)
	if err != nil {
		return &Share{
			JobId:      jobId,
			RemoteAddr: ipAddr,
			Miner:      miner,
			Rig:        rig,

			ErrorCode: types.ErrIncorrectNTimeSize,
		}, minerResult
	}

	// allowed nTime range [GBT's CurTime, submitTime+7s]
	// Update: allowed time range 10 minutes.
	// GBT check is dropped.
	binary.LittleEndian.Uint32(rawTime)
	// nTimeInt := binary.LittleEndian.Uint32(rawTime)
	nTimeInt := binary.BigEndian.Uint32(rawTime)
	if int64(nTimeInt) > submitTime.Unix()+60*20 {
		logger.Log.Error().Msgf("nTime incorrect: expect less %d got %d", submitTime.Unix()+7, nTimeInt)
		return &Share{
			JobId:      jobId,
			RemoteAddr: ipAddr,
			Miner:      miner,
			Rig:        rig,

			ErrorCode: types.ErrNTimeOutOfRange,
		}, minerResult
	}

	if len(hexNonce) != 8 {
		return &Share{
			JobId:      jobId,
			RemoteAddr: ipAddr,
			Miner:      miner,
			Rig:        rig,

			ErrorCode: types.ErrIncorrectNonceSize,
		}, minerResult
	}

	if !job.RegisterSubmit(strconv.FormatUint(uint64(extraNonce1), 16), hexExtraNonce2, hexNTime, hexNonce) {
		return &Share{
			JobId:      jobId,
			RemoteAddr: ipAddr,
			Miner:      miner,
			Rig:        rig,
			ErrorCode:  types.ErrDuplicateShare,
		}, minerResult
	}

	// nonce, err := strconv.ParseInt(hexNonce, 16, 64)
	rawNonce, err := hex.DecodeString(hexNonce)
	if err != nil {
		logger.Log.Error().Err(err).Msg("unable to parse hexNonce")
		return &Share{
			JobId: jobId, RemoteAddr: ipAddr, Miner: miner,
			Rig: rig, ErrorCode: types.ErrIncorrectExtraNonce2Size,
		}, minerResult
	}

	nonce := binary.BigEndian.Uint32(rawNonce)
	timestamp := time.Unix(int64(nTimeInt), 0)

	rawExtraNonce1 := make([]byte, 4)
	binary.LittleEndian.PutUint32(rawExtraNonce1, extraNonce1)

	rawExtraNonce2, err := hex.DecodeString(hexExtraNonce2)
	if err != nil {
		logger.Log.Error().Err(err).Msg("unable to parse hexExtraNonce2")
		return &Share{
			JobId: jobId, RemoteAddr: ipAddr, Miner: miner,
			Rig: rig, ErrorCode: types.ErrIncorrectExtraNonce2Size,
		}, minerResult
	}

	extraNonce := append(rawExtraNonce1, rawExtraNonce2...)
	// extraNonce := append(rawNonce, utils.ReverseBytes(rawExtraNonce2)...)
	logger.Log.Trace().Str("extraNonce", hex.EncodeToString(extraNonce)).Msg("debug extraNonce")

	// VersionBits
	var versionBits uint64
	if versionRolling.Supported {
		versionBits, err = strconv.ParseUint(hexVersionBits, 16, 32)
		if err != nil {
			logger.Log.Error().Err(err).Msg("unable to parse hexVersionBits")
			return &Share{
				JobId: jobId, RemoteAddr: ipAddr, Miner: miner,
				Rig: rig, ErrorCode: types.ErrIncorrectVersionBits,
			}, minerResult
		}

		// Miner can set only bits corresponding to the set bits in the last received mask from the server.
		// This must hold: version_bits & ~last_mask ==  0
		if uint32(versionBits) & ^versionRolling.Mask != 0 {
			return &Share{
				JobId: jobId, RemoteAddr: ipAddr, Miner: miner,
				Rig: rig, ErrorCode: types.ErrIncorrectVersionBitsValue,
			}, minerResult
		}
	}

	job.MinerTask.BitcoinBlock = utils.SetStratumExtraNonce(job.MinerTask.BitcoinBlock, job.MinerTask.BitcoinBlockCoinbaseParts, extraNonce)

	if jm.Config.Log.Debug {
		buf := bytes.NewBuffer(nil)
		job.MinerTask.BitcoinBlock.Header.Serialize(buf)
		buf = bytes.NewBuffer(nil)
		job.MinerTask.BitcoinBlock.Transactions[0].Serialize(buf)
		logger.Log.Info().Str("value", hex.EncodeToString(buf.Bytes())).Msg("Check Job Coinbase")
	}

	validShareFound := false

	minerResult, validShareFound = jm.CheckSolution(job.MinerTask, nonce, timestamp, uint32(versionBits), versionRolling.Mask, jm.Config.EnableHashSorting, jm.Config.HashSortingSlotNumber, clientTarget)

	if validShareFound {
		// logger.Log.Info().Msg("At least one share found")

		// TMP
		// if minerResult.ContainsMinedBitcoinBlock() && minerResult.ContainsMinedBeaconBlock() {
		return &Share{
			JobId:      jobId,
			RemoteAddr: ipAddr,
			Miner:      miner,
			Rig:        rig,
			// TODO other fields
		}, minerResult
		// } else {
		// 	logger.Log.Info().Msg("At least one block mined but hash is incorrect")
		// 	return &Share{ErrorCode: types.ErrBitcoinBlockInvalidHash}, minerResult
		// }
	}

	return &Share{ErrorCode: types.ErrLowDiffShare}, minerResult
}

func (jm *JobManager) CheckSolution(task tasks.MinerTask, nonce uint32, timestamp time.Time, versionBits, lastMask uint32, enableHashSorting bool, hashSortingSlotNumber uint32, clientTarget *big.Int) (tasks.MinerResult, bool) {
	result := tasks.NewMinerResult()

	btcBlock := task.BitcoinBlock
	btcBlock.Header.Nonce = nonce
	btcBlock.Header.Timestamp = timestamp

	// Version rolling
	if versionBits != 0 && lastMask != 0 {
		// nVersion = (job_version & ~last_mask) | (version_bits & last_mask)
		jobVersion := uint32(btcBlock.Header.Version)
		nVersion := (jobVersion & ^versionBits) | (versionBits & lastMask)
		btcBlock.Header.Version = int32(nVersion)
	}

	bitcoinPowHash := btcBlock.Header.BlockHash()
	bitHashRepresentation := pow.HashToBig((*chainhash.Hash)(&bitcoinPowHash))
	validShareFound := false

	// hashSortingValue := utils.HashSortingLastBits(bitHashRepresentation, hashSortingSlotNumber)
	logger.Log.Info().
		Int64("height", task.BitcoinBlockHeight).
		// Uint32("hashSortingValue", hashSortingValue).
		Stringer("pow_hash", bitcoinPowHash).
		Int("txCount", len(btcBlock.Transactions)).
		Msg("Bitcoin block calculated")

	// First check for share client target, which converted from client difficulty.
	if bitHashRepresentation.Cmp(clientTarget) <= 0 {
		validShareFound = true
	}

	if !validShareFound {
		return result, false
	}

	// TODO: decide how to handle share target
	shareTarget := task.ShareTarget
	if bitHashRepresentation.Cmp(&shareTarget) <= 0 {
		validShareFound = true
	}

	// if !validShareFound {
	// 	return result, validShareFound
	// }

	target := task.BitcoinBlockTarget()
	logger.Log.Trace().Str("share target", bitHashRepresentation.Text(16)).Str("bitcoin target", target.Text(16)).Msg("debug bitcoin diff check")
	if bitHashRepresentation.Cmp(&target) <= 0 {
		logger.Log.Info().
			Int64("height", task.BitcoinBlockHeight).
			Stringer("pow_hash", bitcoinPowHash).
			Int("txCount", len(btcBlock.Transactions)).
			Msg("Bitcoin block mined")

		// Move block to the result struct as finalized.
		result.SetBitcoinSolution(&btcBlock)
		validShareFound = true
	}

	if task.BlockFlags&tasks.BeaconFlag == 0 {
		return result, validShareFound
	}

	beaconBlock := task.BeaconBlock.Copy()
	beaconBlock.Header.BeaconHeader().SetBTCAux(utils.BtcBlockToBlockAux(&btcBlock))

	// Due to the hash sorting (see white paper for the details),
	// received block is theoretically applicable only to some very tiny set of shards
	// (or even only one, if shards amount is less than hash sorting number, currently set to 1024).
	HSIndex := jm.hashSortingIndex(bitcoinPowHash)

	logger.Log.Trace().Uint32("HSIndex", HSIndex).Msg("Checking hash sorting value")
	if !jm.hashSortingCorrespondsToCurrentState(HSIndex) {
		return result, validShareFound
	}

	// Sending beacon block to the node.
	if HSIndex == hashSortingBeaconIndex {
		blockMatchesTarget := bitHashRepresentation.Cmp(&task.BeaconTarget) <= 0

		if blockMatchesTarget {
			logger.Log.Info().
				Uint32("hashSortingSlotNumber", hashSortingSlotNumber).
				Uint32("chainID", 0).
				Uint32("hashSortingIndex", HSIndex).
				Int64("height", task.BeaconBlockHeight).
				Stringer("hash", beaconBlock.BlockHash()).
				Stringer("pow_hash", bitcoinPowHash).
				Msg("Beacon block mined with hash sorting")

			// Move block to the result struct as finalized.
			result.SetBeaconSolution(beaconBlock)
			validShareFound = true
		} else {
			logger.Log.Trace().
				Uint32("hashSortingSlotNumber", hashSortingSlotNumber).
				Uint32("chainID", 0).
				Uint32("hashSortingIndex", HSIndex).
				Msg("Beacon block mined with low difficulty")
		}

		// HS index defines beacon as target.
		// No shards could be closed with the same submission,
		// except the case when network is so big that it contains shard with index that is equal to
		// 0 + HashSortingNumber
		HSIndex += jm.Config.HashSortingSlotNumber
	}

	shardIndex := common.ShardID(HSIndex)
	for {
		logger.Log.Trace().Uint32("HSIndex", HSIndex).Msg("Checking shard block")

		// It is safe to read this map concurrently.
		// This is a config, and it could change only in case if whole server configuration is changing,
		// but in this case — coordinator itself would be re-run with the new config instance.
		// (see server implementation starting from the main function).
		_, indexIsRelevant := jm.Config.Shards[shardIndex]
		if !indexIsRelevant {
			// No shard with current index is present.
			// No reason to look for another shards (with bigger index).
			break
		}

		var (
			shardIsPresent bool
			shardTarget    big.Int
			shardCandidate *tasks.ShardTask
		)

		logger.Log.Trace().Uint32("shardIndex", uint32(shardIndex)).Msg("Checking shard block")
		shardCandidate, shardIsPresent = task.ShardsCandidates[shardIndex]
		if shardIsPresent {
			shardTarget = shardCandidate.Target
		}

		if shardIsPresent {
			blockMatchesShardTarget := bitHashRepresentation.Cmp(&shardTarget) <= 0
			logger.Log.Trace().Str("bitHashRepresentation", bitHashRepresentation.Text(16)).Str("shardTarget", shardTarget.Text(16)).Msg("debug shard diff")
			if blockMatchesShardTarget {
				logger.Log.Info().
					Int64("height", shardCandidate.BlockHeight).
					Uint32("shard-id", uint32(shardCandidate.ID)).
					Uint32("hashSortingIndex", HSIndex).
					Stringer("hash", task.BeaconBlock.BlockHash()).
					Stringer("pow_hash", bitcoinPowHash).
					Msg("Shard block mined")
				result.AppendShardSolution(shardCandidate.ID, &shardCandidate.BlockCandidate, beaconBlock)
				validShareFound = true
			}
		}

		// In theory, it is possible, that network would consist more than 1024 shards
		// (hash sorting number), so more than one shard potentially could be closed by the submission.
		shardIndex += common.ShardID(jm.Config.HashSortingSlotNumber)
	}

	return result, validShareFound
}

// Jax.Network uses hash sorting (see white paper for the details how it is implemented).
// For the API to be able to check hash sorting indexes correspondence fast,
// some trivial optimization is used:
// the shards indexes are sorted and then split into non-interpretable ranges, e.g.
// (Range, Range, ... Range), where Range is a combination of 2 numbers: smallest index, and biggest index.
//
// The example:
// Original set of shards: (0,1,2,3,5,6,7,9,11,25)
// The sorted set of ranges {Range(0,3), Range(5,7), Range(9,11), Range(25,25) }
func (jm *JobManager) initOptimizedHashSortingShardsIndexesStorage() {
	totalIndexLength := len(jm.Config.Shards) + 1
	shardsSlotsIndex := make([]uint32, 0, totalIndexLength)

	shardsSlotsIndex = append(shardsSlotsIndex, hashSortingBeaconIndex)
	for _, shardConf := range jm.Config.Shards {
		shardsSlotsIndex = append(shardsSlotsIndex, uint32(shardConf.ID))
	}

	sort.Slice(shardsSlotsIndex, func(i, j int) bool {
		return shardsSlotsIndex[i] < shardsSlotsIndex[j]
	})

	switch len(shardsSlotsIndex) {
	case 0:
		// No shards are present.
		// No index is needed.
		return

	case 1:
		// Only one shard is present.
		jm.hashSortingIndexes = append(jm.hashSortingIndexes, HashSortingIndexesRange{
			SmallestIndex: shardsSlotsIndex[0],
			BiggestIndex:  shardsSlotsIndex[0],
		})
		return

	default:
		lastProcessedIndex := shardsSlotsIndex[0]
		currentRange := HashSortingIndexesRange{
			SmallestIndex: lastProcessedIndex,
		}

		for _, index := range shardsSlotsIndex[1:] {
			if index > (lastProcessedIndex + 1) {
				// Sequence interruption occurred.
				// Current index range must be closed.
				currentRange.BiggestIndex = lastProcessedIndex
				jm.hashSortingIndexes = append(jm.hashSortingIndexes, currentRange)

				// Starting new range.
				currentRange = HashSortingIndexesRange{
					SmallestIndex: index,
				}
			}

			lastProcessedIndex = index
		}

		// Last shards index processing.
		currentRange.BiggestIndex = lastProcessedIndex
		jm.hashSortingIndexes = append(jm.hashSortingIndexes, currentRange)
	}
}

// hashSortingIndex returns HS slot of the shard (or beacon) that corresponds to current hash.
func (jm *JobManager) hashSortingIndex(hash btcdchainhash.Hash) (index uint32) {
	convertedHash, _ := chainhash.NewHash(hash[:])
	hashBig := pow.HashToBig(convertedHash)
	index = pow.HashSortingLastBits(hashBig, jm.Config.HashSortingSlotNumber)
	return
}

// hashSortingCorrespondsToCurrentState returns true if hsIndex falls into current set of shards.
// Does efficient lookup into previously generated hash sorting index.
func (jm *JobManager) hashSortingCorrespondsToCurrentState(hsIndex uint32) bool {
	logger.Log.Trace().Interface("jm.hashSortingIndexes", jm.hashSortingIndexes).Msg("debug jm.hashSortingIndexes")
	for _, indexesRange := range jm.hashSortingIndexes {
		logger.Log.Trace().Uint32("indexesRange.SmallestIndex", indexesRange.SmallestIndex).Uint32("indexesRange.BiggestIndex", indexesRange.BiggestIndex).Msg("debug jm.hashSortingIndexes")

		if hsIndex >= indexesRange.SmallestIndex && hsIndex <= indexesRange.BiggestIndex {
			return true
		}

		if hsIndex < indexesRange.SmallestIndex {
			return false
		}
	}

	return false
}
