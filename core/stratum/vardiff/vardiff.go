// Package vardiff
// Difficulty, Bits and Target
// Difficulty is a measure of how difficult it is to find a hash below a given target.
// difficulty = difficulty_1_target / current_target - (target is a 256 bit number)
//
// Each block stores a packed representation (called "Bits") for its actual hexadecimal target.
// The highest possible target (difficulty 1) is defined as 0x1d00ffff
//  hex target: 0x00ffff * 2**(8*(0x1d - 3)) = 0x00000000FFFF0000000000000000000000000000000000000000000000000000
// It should be noted that pooled mining often uses non-truncated targets, which puts "pool difficulty 1" at
// 0x00000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF

package vardiff

import (
	"math/big"
	"sync"

	"gitlab.com/jaxnet/core/miner/core/logger"
	"gitlab.com/jaxnet/core/miner/core/settings"
	"gitlab.com/jaxnet/core/miner/core/utils"
)

type VarDiff struct {
	mu                                 sync.Mutex
	Enabled                            bool
	FixDiff                            float64
	StartDiff                          float64
	MinDiff                            float64
	MaxDiff                            float64
	CurrDiff                           float64
	NewDiff                            float64
	ClientTarget                       *big.Int
	TargetSharesPerTimeWindow          uint32
	TargetSharesPerTimeWindowThreshold uint32
	sharesPerTimeWindow                uint32
	TimeWindow                         int64 // 30 seconds
	RetargetTimeWindow                 int64 // 90 seconds
	JobPriority                        bool
	StopChan                           chan struct{}
}

func NewVarDiff(options *settings.Configuration) *VarDiff {
	if !options.Stratum.Ports.VarDiff.Enabled {
		return &VarDiff{
			Enabled: false,
		}
	}

	return &VarDiff{
		Enabled:                            true,
		StartDiff:                          options.Stratum.Ports.VarDiff.StartDiff,
		MinDiff:                            options.Stratum.Ports.VarDiff.MinDiff,
		MaxDiff:                            options.Stratum.Ports.VarDiff.MaxDiff,
		TargetSharesPerTimeWindow:          options.Stratum.Ports.VarDiff.TargetSharesPerTimeWindow,
		TargetSharesPerTimeWindowThreshold: options.Stratum.Ports.VarDiff.TargetSharesPerTimeWindowThreshold,
		TimeWindow:                         options.Stratum.Ports.VarDiff.TimeWindow,         // 60 seconds
		RetargetTimeWindow:                 options.Stratum.Ports.VarDiff.RetargetTimeWindow, // 60 seconds
		JobPriority:                        options.Stratum.Ports.VarDiff.JobPriority,
		StopChan:                           make(chan struct{}),
	}
}

func (vd *VarDiff) CalcNextDiff() {
	// Use case with variable difficulty enabled and d={float64} parameter is passed in miner password
	if vd.FixDiff != 0.0 {
		logger.Log.Trace().Msg("VarDiff CalcNextDiff for FixDiff")
		if vd.NewDiff != vd.FixDiff {
			vd.NewDiff = vd.FixDiff
			vd.ClientTarget = utils.Difficulty2Target(big.NewFloat(vd.NewDiff))
		}
		return
	}

	// If miner sent shares within certain range, do not change current difficulty
	minThreshold := vd.TargetSharesPerTimeWindow - vd.TargetSharesPerTimeWindowThreshold
	maxThreshold := vd.TargetSharesPerTimeWindow + vd.TargetSharesPerTimeWindowThreshold

	if vd.sharesPerTimeWindow < minThreshold || vd.sharesPerTimeWindow > maxThreshold {
		var factor, newDiff float64

		// If miner sent 0 shares per time window use fixed factor (current difficulty divided by 2).
		if vd.sharesPerTimeWindow == 0 {
			factor = 0.5
		} else {
			factor = float64(vd.sharesPerTimeWindow) / float64(vd.TargetSharesPerTimeWindow)
		}

		logger.Log.Trace().Float64("vd.CurrDiff", vd.CurrDiff).Uint32("vd.sharesPerTimeWindow", vd.sharesPerTimeWindow).Float64("factor", factor).Msg("VarDiff before")
		newDiff = vd.CurrDiff * factor

		if newDiff < vd.MinDiff {
			newDiff = vd.MinDiff
		}

		if newDiff > vd.MaxDiff {
			newDiff = vd.MaxDiff
		}

		vd.NewDiff = newDiff
		vd.sharesPerTimeWindow = 0
	} else {
		vd.NewDiff = vd.CurrDiff
		logger.Log.Trace().Msg("Shares per time window within allowed range")
	}

	logger.Log.Trace().Float64("vd.CurrDiff", vd.CurrDiff).Float64("vd.NewDiff", vd.NewDiff).Msg("VarDiff after")
}

func (vd *VarDiff) AppendShare(time int64) {
	if vd.FixDiff != 0.0 {
		return
	}

	vd.mu.Lock()
	defer vd.mu.Unlock()

	vd.sharesPerTimeWindow++
	logger.Log.Trace().Int64("time", time).Uint32("vd.sharesPerTimeWindow", vd.sharesPerTimeWindow).Msg("VarDiff AppendShare")
}

func (vd *VarDiff) SetCurrDifficulty(diff float64) {
	vd.mu.Lock()
	defer vd.mu.Unlock()

	vd.CurrDiff = diff
	vd.ClientTarget = utils.Difficulty2Target(big.NewFloat(vd.CurrDiff))
}
