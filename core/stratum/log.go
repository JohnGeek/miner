package stratum

import (
	"github.com/rs/zerolog"
	"gitlab.com/jaxnet/core/miner/core/logger"
)

const loggerComponent = "stratum server"

func (ss *Server) logTrace() *zerolog.Event {
	return logger.Log.Trace().Str("component", loggerComponent)
}

func (ss *Server) logInfo() *zerolog.Event {
	return logger.Log.Info().Str("component", loggerComponent)
}

func (ss *Server) logErr(err error) *zerolog.Event {
	return logger.Log.Err(err).Str("component", loggerComponent)
}
