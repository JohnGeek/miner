/*
 * Copyright (c) 2020 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package tasks

import (
	"math/big"
	"time"

	btcdwire "github.com/btcsuite/btcd/wire"
	"gitlab.com/jaxnet/core/miner/core/common"
	"gitlab.com/jaxnet/core/miner/core/utils"
	"gitlab.com/jaxnet/jaxnetd/types/chainhash"
	mm "gitlab.com/jaxnet/jaxnetd/types/merge_mining_tree"
	"gitlab.com/jaxnet/jaxnetd/types/pow"
	"gitlab.com/jaxnet/jaxnetd/types/wire"
)

const (
	// CoinbaseFlags is added to the coinbase script of a generated block
	// and is used to monitor BIP16 support as well as blocks that are
	// generated via btcd.
	CoinbaseFlags = "/P2SH/jaxnet/"

	// MiningRoundTimeWindow defines how much time would be taken by one miming round.
	// Mining round - it is a calculations cycle, during which miner do not accepts new tasks
	// (does not react on changes in blocks configuration).
	// During this time window the miner is trying to solve the PoC for beacon/shards(s) chain blocks,
	// that has been received on a last update.
	MiningRoundTimeWindow = time.Millisecond * 150
)

type ShardTask struct {
	ID             common.ShardID
	BlockCandidate wire.MsgBlock
	BlockHeight    int64
	Target         big.Int
}

type MinerTaskBlockFlags uint8

const (
	BitcoinFlag MinerTaskBlockFlags = 1 << iota
	BeaconFlag
)

// MinerTask represents a miner task.
// Each task is an independent unit of data that could be stored/processed on its own.
// It is designed to not to share any data with any other components,
// so it can be safely transferred through the goroutines.
type MinerTask struct {
	BeaconBlock       wire.MsgBlock
	BeaconBlockHeight int64
	BeaconHash        chainhash.Hash

	// Represents target of the beacon chain.
	// (this is the main target of the mining process).
	BeaconTarget big.Int

	// Represents targets of the shards.
	// The slice is supposed to be sorted in descendant order.
	// Sorting is needed for the mining process efficiency:
	// during mining, on each hash mined, it is applied to the shards targets,
	// from smallest one to the biggest one.
	// The generated hash would not be applied to the next shard target
	// in case if current one does not suite the generated hash.
	ShardsTargets    []ShardTask
	ShardsCandidates map[common.ShardID]*ShardTask

	BitcoinBlock              btcdwire.MsgBlock
	BitcoinBlockHeight        int64
	BitcoinBlockCoinbaseParts utils.BitcoinBlockCoinbaseParts

	ShareTarget big.Int

	BlockFlags     MinerTaskBlockFlags
	lastExtraNonce uint64
}

func (task *MinerTask) IsNewer(prev *MinerTask, btcMode bool) bool {
	if prev == nil {
		return true
	}

	btcNewer := task.BitcoinBlockHeight > prev.BitcoinBlockHeight
	beaconNewer := task.BeaconBlockHeight > prev.BeaconBlockHeight
	// todo: improve this
	atLeasOneShard := len(task.ShardsTargets) > len(prev.ShardsTargets)

	if btcMode {
		return btcNewer || beaconNewer || atLeasOneShard
	}

	return beaconNewer || atLeasOneShard
}

func (task *MinerTask) BitcoinBlockTarget() big.Int {
	return *pow.CompactToBig(task.BitcoinBlock.Header.Bits)
}

func (task *MinerTask) Validate() error {

	for _, shardBlockTarget := range task.ShardsTargets {
		shardBlock := shardBlockTarget.BlockCandidate
		header := shardBlock.Header
		beaconAux := header.BeaconHeader()
		var position uint32 = uint32(shardBlockTarget.ID) - 1
		exclusiveHash := header.ExclusiveHash()
		tree := mm.NewSparseMerkleTree(beaconAux.Shards())
		err := tree.ValidateShardMerkleProofPath(position, beaconAux.Shards(), header.ShardMerkleProof(),
			exclusiveHash, beaconAux.MergeMiningRoot())

		if err != nil {
			return err
		}

	}
	return nil
}

func (task *MinerTask) UpdateTimeAndMerkleRoots(timestamp time.Time, extraNonce uint64) {
	if task.BlockFlags&BitcoinFlag != 0 {
		task.BitcoinBlock.Header.Timestamp = timestamp
	}
	if task.BlockFlags&BeaconFlag != 0 {
		task.BeaconBlock.Header.SetTimestamp(timestamp)
		if task.lastExtraNonce == 0 || task.lastExtraNonce != extraNonce {
			task.BeaconBlock, _ = utils.UpdateBeaconExtraNonce(task.BeaconBlock, task.BeaconBlockHeight, extraNonce)
		}
	}

	for _, shard := range task.ShardsTargets {
		shard.BlockCandidate.Header.SetTimestamp(timestamp)
		if task.lastExtraNonce == 0 || task.lastExtraNonce != extraNonce {
			shard.BlockCandidate, _ = utils.UpdateShardsMerkleRoot(shard.BlockCandidate,
				shard.BlockHeight, uint32(shard.ID), extraNonce)
		}
	}

	task.lastExtraNonce = extraNonce
}

func (task *MinerTask) UpdateShardsMerkleRoot(extraNonce uint64) {
	for _, shard := range task.ShardsTargets {
		shard.BlockCandidate, _ = utils.UpdateShardsMerkleRoot(shard.BlockCandidate,
			shard.BlockHeight, uint32(shard.ID), extraNonce)
	}
}

// MinerResult represents result of the mining process.
// Could contain mined beacon chain block and/or one or more mined shards blocks.
type MinerResult struct {
	BitcoinBlock btcdwire.MsgBlock
	BeaconBlock  wire.MsgBlock
	ShardsBlocks map[common.ShardID]*wire.MsgBlock
	hasBeacon    bool
	hasBitcoin   bool

	NumHashes uint64
}

// Context represents the mining process state.
// It is used for storing and updating the context of the current mining round.
type Context struct {
	Task       *MinerTask
	Result     *MinerResult
	Ticker     *time.Ticker
	Nonce      uint32
	ExtraNonce uint64
}

func NewMinerResult() MinerResult {
	return MinerResult{
		BitcoinBlock: btcdwire.MsgBlock{},
		BeaconBlock:  wire.EmptyBeaconBlock(),
		ShardsBlocks: make(map[common.ShardID]*wire.MsgBlock),
	}
}

func (mr *MinerResult) SetBitcoinSolution(block *btcdwire.MsgBlock) {
	mr.hasBitcoin = true
	mr.BitcoinBlock = *utils.BitcoinBlockCopy(block)
}

func (mr *MinerResult) SetBeaconSolution(block *wire.MsgBlock) {
	mr.hasBeacon = true
	mr.BeaconBlock = *block.Copy()
}

func (mr *MinerResult) AppendShardSolution(id common.ShardID, shardBlock *wire.MsgBlock, solvedBeaconBlock *wire.MsgBlock) {
	mr.ShardsBlocks[id] = shardBlock.Copy()
	coinbaseAux := wire.CoinbaseAux{}.FromBlock(solvedBeaconBlock, true)
	mr.ShardsBlocks[id].Header.SetBeaconHeader(solvedBeaconBlock.Header.Copy().BeaconHeader(), coinbaseAux)
}

func (mr *MinerResult) ContainsMinedBeaconBlock() bool {
	return mr.hasBeacon
}

func (mr *MinerResult) ContainsMinedBitcoinBlock() bool {
	return mr.hasBitcoin
}

func (mr *MinerResult) ContainsMinedShardsBlocks() bool {
	return len(mr.ShardsBlocks) > 0
}

func (mr *MinerResult) ContainsMinedBlocks(btcMiningEnabled bool) bool {
	return (btcMiningEnabled && mr.hasBitcoin) || mr.hasBeacon || len(mr.ShardsBlocks) > 0
}
