/*
 * Copyright (c) 2020 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package settings

import (
	btcdrpcclient "github.com/btcsuite/btcd/rpcclient"
	"gitlab.com/jaxnet/core/miner/core/common"
	"gitlab.com/jaxnet/core/miner/core/e"
	"gitlab.com/jaxnet/core/miner/core/logger"
	"gitlab.com/jaxnet/jaxnetd/jaxutil"
	"gitlab.com/jaxnet/jaxnetd/network/rpcclient"
)

type Configuration struct {
	Settings

	Shards map[common.ShardID]ShardConfig

	BtcMiningAddress jaxutil.Address
	JaxMiningAddress jaxutil.Address
}

// TotalNetworkShardsCount returns the total amount of shards in the network
// (according to the config).
// The total amount of shards that are listed in config file could differ
// from the total amount of shards that are present in the network,
// e.g. miner could mine shards 100, and 200 and the total amount of shards mined == 2,
// but the total amount of shards in the network, according to this config is 200.
func (s *Configuration) TotalNetworkShardsCount() uint32 {
	return uint32(len(s.Shards))
}

// BeaconRPCConf returns RPC connection parameters of the beacon chain
// (in a format that is appropriate for the RPC-client).
func (s *Configuration) BeaconRPCConf() (conf *rpcclient.ConnConfig) {
	beacon := s.Network.Beacon.Discovering.RPC
	conf = s.defaultRPCConfig()
	conf.Host = beacon.Network.Interface()
	conf.User = beacon.User
	conf.Pass = beacon.Pass
	return
}

// ShardRPCConf returns RPC connection parameters of the specified shard chain.
// (in a format that is appropriate for the RPC-client).
func (s *Configuration) ShardRPCConf(shardID common.ShardID) (conf *rpcclient.ConnConfig, err error) {
	for _, shard := range s.Shards {
		if shard.ID == shardID {
			rpc := shard.RPC
			conf = s.defaultRPCConfig()
			conf.Host = rpc.Network.Interface()
			conf.User = rpc.User
			conf.Pass = rpc.Pass
			return
		}
	}

	err = e.ErrInvalidShardID
	return
}

// defaultRPCConfig returns RPC connection parameters
// that are common for beacon chain and all shard chains.
func (s *Configuration) defaultRPCConfig() *rpcclient.ConnConfig {
	return &rpcclient.ConnConfig{
		Params:       s.Network.Name,
		HTTPPostMode: true, // Bitcoin core only supports HTTP POST mode
		DisableTLS:   true, // Bitcoin core does not provide TLS by default
	}
}

// BitcoinRPCConf returns RPC connection parameters of the bitcoin node
// (in a format that is appropriate for the RPC-client).
func (s *Configuration) BitcoinRPCConf() (conf *btcdrpcclient.ConnConfig) {
	bitcoin := s.Network.Bitcoin
	conf = s.defaultBitcoinRPCConfig()
	conf.Params = bitcoin.Params
	conf.Host = bitcoin.Network.Interface()
	conf.User = bitcoin.User
	conf.Pass = bitcoin.Pass
	conf.DisableTLS = bitcoin.Network.DisableTLS
	conf.ExtraHeaders = bitcoin.ExtraHeaders

	return
}

// defaultBitcoinRPCConfig returns RPC connection parameters
// that are common for bitcoin chain.
func (s *Configuration) defaultBitcoinRPCConfig() *btcdrpcclient.ConnConfig {
	return &btcdrpcclient.ConnConfig{
		Params:       s.Network.Bitcoin.Params,
		HTTPPostMode: true, // Bitcoin core only supports HTTP POST mode
	}
}

type Settings struct {
	Log logger.LogParams `yaml:"log"`

	NumCPU                int    `yaml:"num_cpu"`
	EnablePprof           bool   `yaml:"enable_pprof"`
	EnableBTCMining       bool   `yaml:"enable_btc_mining"`
	EnableBCHMode         bool   `yaml:"enable_bch_mode"`
	EnableHashSorting     bool   `yaml:"-"`
	HashSortingSlotNumber uint32 `yaml:"-"`
	ChainIDCount          uint32 `yaml:"chain_id_count"`

	BurnBtcReward    bool   `yaml:"burn_btc"`
	BtcMiningAddress string `yaml:"btc_mining_address"`
	JaxMiningAddress string `yaml:"jax_mining_address"`

	Network Network `yaml:"network"`
	Stratum Stratum `yaml:"stratum"`
	Redis   Redis   `yaml:"redis"`
}

type Network struct {
	Name string `yaml:"name"`

	Beacon struct {
		SetExpansionFlagInBeaconHeader bool `yaml:"set_expansion_flag_in_beacon_header"`
		Discovering                    struct {
			RPC RPCConfig `yaml:"rpc"`
		} `yaml:"discovering"`
	} `yaml:"beacon"`

	Shards struct {
		Discovering struct {
			Dynamic struct {
				Mode      string `yaml:"mode"`
				RoundTime string `yaml:"round-time"`
			} `yaml:"dynamic"`

			Static struct {
				Credentials []ShardConfig `yaml:"credentials"`
				Forbidden   string        `yaml:"forbidden"`
			} `yaml:"static"`
		} `yaml:"discovering"`
	} `yaml:"shards"`

	Bitcoin BitcoinParams `yaml:"bitcoin"`
}

type BitcoinParams struct {
	RPCConfig `yaml:",inline"`
	Params    string `yaml:"params"`
}

type Stratum struct {
	Banning               StratumBanning  `yaml:"banning"`
	JobRebroadcastTimeout int             `yaml:"job_rebroadcast_timeout"`
	TcpProxyProtocol      bool            `yaml:"tcp_proxy_protocol"`
	ConnectionTimeout     int             `yaml:"connection_timeout"`
	Ports                 StratumListener `yaml:"ports"`
	APIServer             APIServerConfig `yaml:"api"`
	UpdateBlockToken      string          `yaml:"update_block_token"`
}

type StratumBanning struct {
	Enabled        bool    `yaml:"enabled"`
	Time           int     `yaml:"time"`
	InvalidPercent float64 `yaml:"invalid_percent"`
	CheckThreshold uint64  `yaml:"check_threshold"`
	PurgeInterval  int     `yaml:"purge_interval"`
}

type StratumListener struct {
	Port    string `yaml:"port"`
	VarDiff struct {
		StartDiff                          float64 `yaml:"start_diff"`
		MinDiff                            float64 `yaml:"min_diff"`
		MaxDiff                            float64 `yaml:"max_diff"`
		TargetSharesPerTimeWindow          uint32  `yaml:"target_shares_per_time_window"`
		TargetSharesPerTimeWindowThreshold uint32  `yaml:"target_shares_per_time_window_threshold"`
		TimeWindow                         int64   `yaml:"time_window"`
		RetargetTimeWindow                 int64   `yaml:"retarget_time_window"`
		JobPriority                        bool    `yaml:"job_priority"`
		Enabled                            bool    `yaml:"enabled"`
	} `yaml:"var_diff"`
	TLS bool `yaml:"tls"`
}

type APIServerConfig struct {
	Host                 string `yaml:"host"`
	Port                 string `yaml:"port"`
	EnableHealthEndpoint bool   `yaml:"enable_health_endpoint"`
	EnableMetrics        bool   `yaml:"enable_metrics"`
}

type Redis struct {
	Enabled bool   `yaml:"enabled"`
	Host    string `yaml:"host"`
	Port    string `yaml:"port"`
	DB      int    `yaml:"db"`
}
