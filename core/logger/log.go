/*
 * Copyright (c) 2020 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package logger

import (
	"io"
	"os"
	"path"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gopkg.in/natefinch/lumberjack.v2"
)

type Logger struct {
	*zerolog.Logger
}

var (
	Log *Logger
)

type LogParams struct {
	JSONMode      bool   `yaml:"json_mode"`
	Debug         bool   `yaml:"debug"`
	OutputFile    string `yaml:"output_file"`
	Level         string `yaml:"level"`
	DisableStdOut bool   `yaml:"disable_std_out"`
}

type config struct {
	ConsoleLoggingEnabled bool
	JSONMode              bool
	LogLevel              string

	FileLoggingEnabled bool
	Directory          string
	Filename           string
	MaxSizeMB          int
	MaxBackups         int
	MaxAgeDays         int
}

var defaultConfig = config{
	ConsoleLoggingEnabled: false,
	FileLoggingEnabled:    false,
	LogLevel:              "info",

	Directory:  "./",
	Filename:   "",
	MaxSizeMB:  100,
	MaxBackups: 10,
	MaxAgeDays: 7,
}

func Init(conf LogParams) {
	cfg := defaultConfig

	if conf.OutputFile != "" {
		cfg.Filename = conf.OutputFile
		cfg.FileLoggingEnabled = true
	}

	cfg.LogLevel = conf.Level
	cfg.JSONMode = conf.JSONMode
	cfg.ConsoleLoggingEnabled = !conf.DisableStdOut

	Log = configure(cfg)
}

// configure sets up the logging framework
//
// In production, the container logs will be collected and file logging should be disabled. However,
// during development it's nicer to see logs as text and optionally write to a file when debugging
// problems in the containerized pipeline
//
// The output log file will be located at /var/log/service-xyz/service-xyz.log and
// will be rolled according to configuration set.
func configure(config config) *Logger {
	var writers []io.Writer

	if config.ConsoleLoggingEnabled && !config.JSONMode {
		writers = append(writers, zerolog.ConsoleWriter{Out: os.Stderr})
	}
	if config.ConsoleLoggingEnabled && config.JSONMode {
		writers = append(writers, os.Stderr)
	}
	if config.FileLoggingEnabled {
		writers = append(writers, newRollingFile(config))
	}

	mw := io.MultiWriter(writers...)
	logger := zerolog.New(mw).With().Timestamp().Logger()

	// logger.Trace().
	// 	Bool("fileLogging", config.FileLoggingEnabled).
	// 	Bool("jsonLogOutput", config.JSONMode).
	// 	Str("logDirectory", config.Directory).
	// 	Str("fileName", config.Filename).
	// 	Str("logLevel", config.LogLevel).
	// 	Int("maxSizeMB", config.MaxSizeMB).
	// 	Int("maxBackups", config.MaxBackups).
	// 	Int("maxAgeInDays", config.MaxAgeDays).
	// 	Msg("logging configured")

	switch config.LogLevel {
	case "panic":
		zerolog.SetGlobalLevel(zerolog.PanicLevel)
	case "fatal":
		zerolog.SetGlobalLevel(zerolog.FatalLevel)
	case "error":
		zerolog.SetGlobalLevel(zerolog.ErrorLevel)
	case "warn":
		zerolog.SetGlobalLevel(zerolog.WarnLevel)
	case "info":
		zerolog.SetGlobalLevel(zerolog.InfoLevel)
	case "debug":
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	case "trace":
		zerolog.SetGlobalLevel(zerolog.TraceLevel)
	default:
		zerolog.SetGlobalLevel(zerolog.InfoLevel)
	}

	zerolog.TimeFieldFormat = zerolog.TimeFormatUnixMs

	return &Logger{Logger: &logger}
}

func newRollingFile(config config) io.Writer {
	if err := os.MkdirAll(config.Directory, 0744); err != nil {
		log.Error().Err(err).Str("path", config.Directory).Msg("can't create log directory")
		return nil
	}

	return &lumberjack.Logger{
		Filename:   path.Join(config.Directory, config.Filename),
		MaxBackups: config.MaxBackups, // files
		MaxSize:    config.MaxSizeMB,  // megabytes
		MaxAge:     config.MaxAgeDays, // days
	}
}
