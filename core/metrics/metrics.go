package metrics

import (
	"regexp"
	"sync"

	"github.com/prometheus/client_golang/prometheus"
)

// MKey type for metrics
type MKey string

type Collector struct {
	gauges      map[MKey]prometheus.Gauge
	gaugeVecs   map[MKey]*prometheus.GaugeVec
	counterVecs map[MKey]*prometheus.CounterVec
	mutex       sync.Mutex
}

const (
	OrphanBlocks      MKey = "orphan_blocks"
	SubmittedShares   MKey = "submitted_shares"
	SubmittedJobs     MKey = "submitted_jobs"
	SubmittedBlocks   MKey = "submitted_blocks"
	WorkerConnTimeout MKey = "worker_conn_timeout"
)

var (
	enabled   bool
	collector Collector
)

func Init() {
	enabled = true
	collector = Collector{
		gauges:      make(map[MKey]prometheus.Gauge),
		gaugeVecs:   make(map[MKey]*prometheus.GaugeVec),
		counterVecs: make(map[MKey]*prometheus.CounterVec),
	}
	RegisterCounterVec(SubmittedJobs, nil)
	RegisterGaugeVec(SubmittedShares, []string{"worker", "valid_shares"})
	RegisterCounterVec(OrphanBlocks, []string{"chain_id"})
	RegisterCounterVec(SubmittedBlocks, []string{"chain_id"})
	RegisterCounterVec(WorkerConnTimeout, []string{"worker"})
}

// IncGauge increments the Gauge by 1. Use AddGauge to increment it by arbitrary
// values.
func IncGauge(name MKey) {
	if !enabled {
		return
	}
	collector.gauges[name].Inc()
}

// DecGauge decrements the Gauge by 1. Use SubGauge to decrement it by arbitrary
// values.
func DecGauge(name MKey) {
	if !enabled {
		return
	}
	collector.gauges[name].Dec()
}

// AddGauge adds the given value to the Gauge. (The value can be negative,
// resulting in a decrease of the Gauge.)
func AddGauge(name MKey, val float64) {
	if !enabled {
		return
	}
	collector.gauges[name].Add(val)
}

// SubGauge subtracts the given value from the Gauge. (The value can be
// negative, resulting in an increase of the Gauge.)
func SubGauge(name MKey, val float64) {
	if !enabled {
		return
	}
	collector.gauges[name].Sub(val)
}

// SetGauge sets the Gauge to an arbitrary value.
func SetGauge(name MKey, val float64) {
	if !enabled {
		return
	}
	collector.gauges[name].Set(val)
}

// SetGaugeVec sets the Gauge to an arbitrary value.
func SetGaugeVec(name MKey, labels map[string]string, val float64) {
	if !enabled {
		return
	}
	collector.gaugeVecs[name].With(labels).Set(val)
}

// IncCounterVec increments the CounterVec by 1. Use AddCounterVec to increment it by arbitrary value.
func IncCounterVec(name MKey, labels map[string]string) {
	if !enabled {
		return
	}
	collector.counterVecs[name].With(labels).Inc()
}

// AddCounterVec increments the CounterVec by arbitrary value.
func AddCounterVec(name MKey, labels map[string]string, val float64) {
	if !enabled {
		return
	}
	collector.counterVecs[name].With(labels).Add(val)
}

func RegisterGauge(name MKey) {
	if !enabled {
		return
	}

	// Sanitize metrics name
	var re = regexp.MustCompile(`[^a-zA-Z_:0-9]+`)
	sName := re.ReplaceAllString(string(name), `_`)

	gauge := prometheus.NewGauge(
		prometheus.GaugeOpts{
			Name:        sName,
			ConstLabels: map[string]string{},
		})
	prometheus.MustRegister(gauge)

	collector.mutex.Lock()
	collector.gauges[name] = gauge
	collector.mutex.Unlock()
}

func RegisterCounterVec(name MKey, labels []string) {
	if !enabled {
		return
	}

	// Sanitize metrics name
	var re = regexp.MustCompile(`[^a-zA-Z_:0-9]+`)
	sName := re.ReplaceAllString(string(name), `_`)

	counterVec := prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: sName,
			Help: "",
		},
		labels,
	)
	prometheus.MustRegister(counterVec)

	collector.mutex.Lock()
	collector.counterVecs[name] = counterVec
	collector.mutex.Unlock()
}

func RegisterGaugeVec(name MKey, labels []string) {
	if !enabled {
		return
	}

	// Sanitize metrics name
	var re = regexp.MustCompile(`[^a-zA-Z_:0-9]+`)
	sName := re.ReplaceAllString(string(name), `_`)

	gaugeVec := prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: sName,
			Help: "",
		},
		labels,
	)
	prometheus.MustRegister(gaugeVec)

	collector.mutex.Lock()
	collector.gaugeVecs[name] = gaugeVec
	collector.mutex.Unlock()
}

func RegisterGaugeCached(name MKey) {
	if !enabled {
		return
	}

	if _, exists := collector.gauges[name]; !exists {
		RegisterGauge(name)
	}
}

func RegisterGauges(names ...MKey) {
	if !enabled {
		return
	}

	for _, name := range names {
		gauge := prometheus.NewGauge(
			prometheus.GaugeOpts{
				Name:        string(name),
				ConstLabels: map[string]string{},
			})
		prometheus.MustRegister(gauge)
		collector.gauges[name] = gauge
	}
}

func RegisterGaugesWithOpts(gOpts []prometheus.GaugeOpts) {
	if !enabled {
		return
	}

	for ind := range gOpts {
		gauge := prometheus.NewGauge(gOpts[ind])
		prometheus.MustRegister(gauge)
		collector.gauges[MKey(gOpts[ind].Name)] = gauge
	}
}
