package storage

import (
	"context"
	"errors"

	"github.com/go-redis/redis/v8"
	"gitlab.com/jaxnet/core/miner/core/logger"
	"gitlab.com/jaxnet/core/miner/core/settings"
)

type Storage struct {
	rdb *redis.Client
	ctx context.Context
}

var (
	enabled bool
	storage Storage
	// To reduce allocations in disabled mode return one time allocated empty map
	results map[string]string
)

const (
	OrphanBlocks = "orphan_blocks"
)

func Stop() error {
	if enabled {
		err := storage.rdb.Close()
		if err != nil {
			return err
		}

		logger.Log.Trace().Msg("Gracefully closed Redis connection")
		return nil
	}

	logger.Log.Trace().Msg("Stopped storage in disabled mode")
	return nil
}

// New populates new Redis storage singleton
func New(conf *settings.Configuration) error {
	if !conf.Redis.Enabled {
		enabled = false
		results = make(map[string]string)
		logger.Log.Trace().Msg("Started storage in disabled mode")
		return nil
	}

	if conf.Redis.Host == "" {
		return errors.New("redis host is not set")
	}

	if conf.Redis.Port == "" {
		return errors.New("redis port is not set")
	}

	addr := conf.Redis.Host + ":" + conf.Redis.Port
	rdb := redis.NewClient(&redis.Options{
		Addr:     addr,
		Password: "",
		DB:       conf.Redis.DB,
	})

	ctx := context.Background()
	_, err := rdb.Ping(ctx).Result()
	if err != nil {
		return errors.New("unable to connect to redis server")
	}

	// Populate storage
	storage = Storage{
		ctx: ctx,
		rdb: rdb,
	}

	enabled = true

	logger.Log.Trace().Str("host", conf.Redis.Host).Str("port", conf.Redis.Port).Msg("Redis connection established")
	return nil
}

// HIncr is a wrapper for HIncrBy Redis command
func HIncr(hKey, hValue string) error {
	if !enabled {
		return nil
	}

	err := storage.rdb.HIncrBy(storage.ctx, hKey, hValue, 1).Err()
	if err != nil {
		return err
	}
	return nil
}

// HGetAll is a wrapper for HGetAll Redis command
func HGetAll(hKey string) (map[string]string, error) {
	if !enabled {
		return results, nil
	}

	result, err := storage.rdb.HGetAll(storage.ctx, hKey).Result()
	if err != nil {
		return nil, err
	}
	return result, nil
}
